<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('bidang_usaha', 'API\Api@bidang_usaha');
Route::get('kabupaten_kota', 'API\Api@kabupaten_kota');
Route::post('/pelatihan/store', 'API\PelatihanApi@store');
Route::post('/data_code/store', 'API\DataCodeApi@store');
