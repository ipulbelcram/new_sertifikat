<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PekerjaanJabatan extends Model
{
    protected $table = 'pekerjaan_jabatan';

    protected $fillable = [
        'pekerjaan_jabatan',
    ];

    public function data_code()
    {
        return $this->hasMany('App\DataCode','pekerjaan_jabatan');
    }
}
