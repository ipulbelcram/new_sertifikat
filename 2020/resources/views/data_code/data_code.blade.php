@extends('template')
@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2 class="text-uppercase">Biodata</h2>
</div>
{!! Form::open(['url' => 'pelatihan/create_data', 'files' => 'true']) !!}
    @include('data_code/form_data_code')
{!! Form::close() !!}

@endSection

@section('script')
    <script src="{{ asset('js/dataCodeForm.js') }}"></script>
@endSection

