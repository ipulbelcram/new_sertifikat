<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CodePelatihan;
use App\Pelatihan;
use App\DataCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class CodePelatihanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('administrator', ['only' => [
            'updateQrCode',
        ]]);
        $this->middleware('notAdminStatistik', ['except' => [
            'updateQrCode',
        ]]);
    }
    
    protected function updateQrCode($data_code_id, $currentpage) {
        $cek_code_pelatihan_tidak = CodePelatihan::where(['data_code_id' => $data_code_id, 'status' => 'T'])->count();
        if($cek_code_pelatihan_tidak >= 1) {
            $data_code = DataCode::findOrFail($data_code_id);

            $data_code->update();
    
            $code_pelatihan = $data_code->code_pelatihan;
            $code_pelatihan->status = "Y";
            $data_code->code_pelatihan()->save($code_pelatihan);
    
            $pelatihan_id = $data_code->pelatihan_id;
            return redirect()->route('data_peserta_pelatihan',[$pelatihan_id, 'page' => $currentpage]);
        } else {
            $where = [
                'data_code_id' => $data_code_id,
                'status' => 'Y',
            ];
            $cek_kode_pelatihan = CodePelatihan::where($where)->count();
            if($cek_kode_pelatihan >= 1) {
                echo "<script>alert('QrCode Sudah Di Buat');history.go(-1);</script>";
            } else {
                $tanggal_sekarang = date('Y-m-d');
                $code_pelatihan = CodePelatihan::where('tanggal_input', $tanggal_sekarang)->select('no_urut')->max('no_urut');
                if(empty($code_pelatihan)) {
                    $no_urut = 1;
                } else {
                    $no_urut = $code_pelatihan+1;
                }
                $data_code = DataCode::where('id', $data_code_id)->get();
                $code_user =  $data_code[0]->pelatihan[0]->user->code_user;
    
                $input['data_code_id'] = $data_code_id;
                $input['code_user'] = $code_user;
                $input['tanggal_input'] = $tanggal_sekarang;
                $input['no_urut'] = $no_urut;
                $input['status'] = "Y";
                CodePelatihan::create($input);
    
                $pelatihan_id = $data_code[0]->pelatihan_id;
                return redirect()->route('data_peserta_pelatihan',[$pelatihan_id, 'page' => $currentpage]);
            }
        }
    }

    public function codeDetail($id) {
        $data_code_pelatihan = CodePelatihan::where('code_pelatihan', $id)->get();
        return view('codePelatihan/QrCode', compact('data_code_pelatihan'));
    }

    public function print($data_code_id, $status_tanggal) {
        $data_code_pelatihan = CodePelatihan::where(['data_code_id'=> $data_code_id, 'status' => 'Y'])->get();
        if($data_code_pelatihan->count() >= 1) {
            foreach($data_code_pelatihan as $code_pelatihan) {
                $no_urut = sprintf("%04d", $code_pelatihan->no_urut);
                list($tahun, $bulan, $tanggal) = explode("-", $code_pelatihan->tanggal_input);
                $QrCode = $code_pelatihan->code_user.''.$tahun.''.$bulan.''.$tanggal.''.$no_urut;
            }
            $data_dataCode =  DataCode::where('id', $data_code_id)->get();
            $tanggal_status = $status_tanggal;
            return view('codePelatihan/print', compact('QrCode', 'data_dataCode', 'tanggal_status'));
        } else {
            echo "<script>alert('QrCode Belum Di Buat');window.close();</script>";
        }
    }

    public function destroy($id, $code_pelatihan)
    {
        $data_code = DataCode::find($code_pelatihan);
        $data_code_pelatihan = CodePelatihan::find($id);
        $pelatihan_id = $data_code_pelatihan->pelatihan_id;

        if(isset($data_code)) {
            $data_code->delete();
            $data_code_pelatihan->delete();
        } else {
            $data_code_pelatihan->delete();
        }

        return redirect()->route('codePelatihan', [$pelatihan_id]);
    }

    public function search(Request $request)
    {
        $cari_code = $request->cari_code;
        $code_user = Auth::user()->code_user;
        if($code_user == "Z") {
            $list_code = DB::table('vw_all_data_code')->where('nama','like','%'.$cari_code.'%')->paginate();
        } else {
            $list_code = DB::table('vw_all_data_code')->where([
                ['nama', 'like', '%'.$cari_code.'%'],
                ['code_user', '=', $code_user],
            ])->paginate();
        }
        $list_code->appends(['cari_code' => $cari_code]);
        return view('codePelatihan/index', ['list_code' => $list_code]);
    }
}
