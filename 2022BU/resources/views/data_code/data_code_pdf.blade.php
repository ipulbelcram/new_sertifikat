<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
    <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'Vivaldi Italic';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('css/font/VIVALDII.woff') }}") format('woff');
        }

        @font-face {
            font-family: 'Tahoma';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('css/font/tahoma.woff') }}") format('woff');
        }
        body {
            background:transparent;
            margin:0;
        }
        .img-background {
            position: relative;
            z-index: 1;
            top: 0;
            width: 100%;
        }
        .isi-content{
            position:absolute;
            top:120px;
            z-index:2;
            width:100%;
        }
        .tahoma {
            font-family: "Tahoma";
            font-size: 15px;
            font-weight: bold;
        }

        .vivaldi {
            font-family: "Vivaldi Italic";
            font-size: 43px;
            font-weight: bold;
        }

        .font-pelatihan {
            font-size: 19px !important;
            font-weight: bold;
        }

        .upercase {
            text-transform: uppercase;
        }

        @page {
            size: auto;
        }

        .foto {
            border: 1px solid #000000;
            width: 75px;
            height: 100px;
        }

        .code {
            margin-top: -40px;
            margin-left: 200px;
        }

        .qrcode {
            padding-top: 25px;
            padding-left: 10px;
            margin-bottom: -20px;
        }

        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>

</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
                <div id="file-name">{{ $data_code->nik.'_'.$data_code->nama}}</div>
            </div>
        </div>
    </div>
    <div class="d-none">
        <div id="content-sertifikat">
            @if($status == 'Y') 
            <img class="img-background" src="{{ asset('images/back_sertifikat.png') }}" style="width:99.9%">
            @endif
            <div class="{{ ($status == 'Y') ? 'isi-content' : '' }}">
                <div class="text-center col-lg-10 d-block mx-auto" style="margin-top:110px;">
                    <div>
                        @if($status == 'Y')
                            @if(substr($data_code->foto, 0, 4) == 'http')
                                <img class="foto d-block mx-auto" src="{{ $data_code->foto }}" alt="" style="border:none !important">
                            @else
                                <img class="foto d-block mx-auto" src="{{ asset('foto/'.$data_code->foto) }}" alt="" style="border:none !important">
                            @endif
                        @elseif($status == 'N')
                            <div class="foto d-block mx-auto" style="width:60px !important; height:80px !important"></div>
                        @endif
                    </div>
                    
        
                    <div class="tahoma" style="margin-top:15px;">Diberikan Kepada :</div> 
                    <div class="vivaldi" style="margin-top:-9px;">{{ $data_code->nama }}</div>
                    <div class="tahoma" style="margin-top:-3px;">Telah Mengikuti :</div>
                    <div class="tahoma font-pelatihan upercase">{{ $data_code->nama_pelatihan }}</div>
                    <div class="tahoma">Pada Tanggal {{ strftime("%d %B", strtotime($data_code->tanggal_mulai)) }} s.d {{ strftime("%d %B %Y", strtotime($data_code->tanggal_selesai)) }}</div>
                    
                    <div class="tahoma">Di <span class="text-capitalize">{{ $data_code->kabupaten_kota }}</span> Provinsi {{ $data_code->provinsi }} </div>
                    <div class="tahoma">Diselenggarakan oleh :</div>
                    <div class="tahoma">{{ $data_code->unit }}</div>
                </div>
                <br>
                <div class="float-left">
                    @php
                        $link = "http://$_SERVER[HTTP_HOST]/2021/pelatihan/detail_data_code/".$data_code->qr_code_no;
                    @endphp
                    <div class="code text-center">
                        <div class="qrcode" style="">{!! QrCode::size(100)->generate($link) !!}</div>
                        <div>
                            <p style="white-space: pre-line; line-height: 0.8;">
                                <small style="font-size:10px">Sertifikat Sudah Diotorisasi</small>
                                <small style="font-size:10px">Secara Elektronik</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="float-right ttd-margin" style="margin-right: 155px;">
                    <div class="col-lg-12 text-center">
                        <div class="tahoma">Jakarta, {{ strftime("%d %B %Y", strtotime($data_code->tanggal_selesai)) }}</div>
                        <div class="tahoma">{{ $data_code->unit }}</div>
                        {{-- <img class="img-fluid" src="{{ asset('images/ttd/cap.png') }}" width="30%" style="position:absolute; right:50px; top:10px;"> --}}
                        <img class="img-fluid" src="{{ asset('images/ttd/cap.png') }}" width="90" style="position:absolute; right:127px; top:10px; z-index: 99;">
	                    <img class="img-fluid" src="{{ asset($data_code->ttd_path) }}" width="130" style="position:absolute; right:50px; top:0px;">
                        <br><br><br>
                        <div class="tahoma" style="margin-top: -20px;">{{ $data_code->name }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/html2pdf.js') }}"></script>
    <script>
        $(window).load(function() {
            let filename =$("#file-name").text() + '.pdf'
            let element = $('#content-sertifikat').html();
            console.log(element)
            let opt = {
                margin: [0, 0],
                filename: filename,
                image: { type: 'jpeg', quality: 0.5 },
                html2canvas: { scale: 4, useCORS: true },
                jsPDF: { unit: 'in', format: 'a4', orientation: 'landscape' }
            }

            html2pdf().from(element).set(opt).toPdf().get('pdf').then(function(pdf) {
                var totalPages = pdf.internal.getNumberOfPages();
                for (i = 1; i <= totalPages; i++) {
                    pdf.setPage(i);
                    pdf.setFontSize(9);
                    pdf.text(i + ' / ' + totalPages, (pdf.internal.pageSize.getWidth() - 0.8), (pdf.internal.pageSize.getHeight() - 0.8));
                }
            }).save();
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        });
    </script>
</body>
</html>