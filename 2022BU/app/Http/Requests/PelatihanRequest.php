<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PelatihanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->metode_pelaksanaan_id == 153) {
            $penanggung_jawab_swakelola = 'required|string';
            $pelaksana_swakelola = 'required|string';
            $no_hp_pelaksana_swakelola = 'required|numeric';

            $nama_perusahaan = 'nullable|string';
            $direktur_utama = 'nullable|string';
            $no_hp = 'nullable|numeric';
            $penanggung_jawab_kontraktual = 'nullable|string';
            $pelaksana_kontraktual = 'nullable|string';
            $no_hp_pelaksana_kontraktual = 'nullable|numeric';
        } else {
            $penanggung_jawab_swakelola = 'nullable|string';
            $pelaksana_swakelola = 'nullable|string';
            $no_hp_pelaksana_swakelola = 'nullable|numeric';

            $nama_perusahaan = 'required|string';
            $direktur_utama = 'required|string';
            $no_hp = 'required|numeric';
            $penanggung_jawab_kontraktual = 'required|string';
            $pelaksana_kontraktual = 'required|string';
            $no_hp_pelaksana_kontraktual = 'required|numeric';
        }
        return [
            'user_id' => 'required',
            'nama_pelatihan' => 'required|string',
            'tempat' => 'required|string',
            'tanggal_mulai' => 'required|date',
            'tanggal_selesai' => 'required|date',
            'provinsi_id' => 'required',
            'kabupaten_kota_id' => 'required|numeric',
            'dalam_rangka_id' => 'required|numeric|exists:params,id',
            'dalam_rangka_des' => 'required|string',
            'dukungan_sektor_id' => 'required|numeric|exists:params,id',
            'metode_pelaksanaan_id' => 'required|numeric|exists:params,id',

            'penanggung_jawab_swakelola' => $penanggung_jawab_swakelola,
            'pelaksana_swakelola' => $pelaksana_swakelola,
            'no_hp_pelaksana_swakelola' => $no_hp_pelaksana_swakelola,

            'nama_perusahaan' => $nama_perusahaan,
            'direktur_utama' => $direktur_utama,
            'no_hp' => $no_hp,
            'penanggung_jawab_kontraktual' => $penanggung_jawab_kontraktual,
            'pelaksana_kontraktual' => $pelaksana_kontraktual,
            'no_hp_pelaksana_kontraktual' => $no_hp_pelaksana_kontraktual,
        ];
    }
}
