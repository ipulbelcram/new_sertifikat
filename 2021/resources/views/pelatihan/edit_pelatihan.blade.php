@extends('template')
@section('content')
{!! Form::model($data_pelatihan, ['method'=>'PATCH', 'action'=>['PelatihanController@update', $data_pelatihan->id]]) !!}
    @include('pelatihan/form')
{!! Form::close() !!}
@endSection
@section('script')
    <script src="{{ asset('js/pelatihanForm.js') }}"></script>
@endSection