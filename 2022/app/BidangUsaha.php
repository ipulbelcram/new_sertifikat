<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidangUsaha extends Model
{
    protected $table = 'bidang_usaha';

    protected $fillable = [
        'bidang_usaha',
    ];
}
