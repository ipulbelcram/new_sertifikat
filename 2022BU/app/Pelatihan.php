<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    protected $table = 'pelatihan';
    protected $fillable = [
        'user_id',
        'nama_pelatihan',
        'tempat',
        'tanggal_mulai',
        'tanggal_selesai',
        'provinsi_id',
        'kabupaten_kota_id',
        'dalam_rangka_id',
        'dalam_rangka_des',
        'dukungan_sektor_id',
        'metode_pelaksanaan_id',
        'penanggung_jawab_swakelola',
        'pelaksana_swakelola',
        'no_hp_pelaksana_swakelola',
        'nama_perusahaan',
        'direktur_utama',
        'no_hp',
        'penanggung_jawab_kontraktual',
        'pelaksana_kontraktual',
        'no_hp_pelaksana_kontraktual',
        'status',
        'ref_asn_pelatihan_id',
    ];

    protected $dates = ['tanggal_mulai', 'tanggal_selesai'];

    public function provinsi()
    {
    	return $this->belongsTo('App\Provinsi', 'provinsi_id');
    }

    public function kabupaten_kota()
    {
        return $this->belongsTo('App\KabKota', 'kabupaten_kota_id');
    }

    public function data_code()
    {
        return $this->belongsTo('App\DataCode', 'id','pelatihan_id');
    }

    public function data_code2()
    {
        return $this->hasMany('App\DataCode', 'pelatihan_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function jenis_pelatihan()
    {
        return $this->belongsTo('App\Params', 'jenis_pelatihan_id');
    }
}
