<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <link rel="icon" href="{{ asset('foto/logo_sertifikat_icon.ico') }}">
    <title>Sertifikat</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <style>
        .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      [role="main"] {
        padding-top: 15px !important; /* Space for fixed navbar */
      }
      
      @media (min-width: 768px) {
        [role="main"] {
          padding-top: 13px !important; /* Space for fixed navbar */
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">

<body>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                        <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                    </div>
                </div>
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">DETAIL PESERTA</h2>
                </div>
                <div class="row">
                    <div class="col-md-3 pt-3 pb-2 mb-3 border-bottom">
                        @if(!empty($data_code_all[0]['foto']))
                            @if(substr($data_code_all[0]['foto'], 0, 4) == 'http')
                            <img src="{{ $data_code_all[0]['foto'] }}" class="img-fluid d-block mx-auto" alt="" >
                            @else
                            <img src="{{ asset('foto/'.$data_code_all[0]['foto']) }}" class="img-fluid d-block mx-auto" alt="" >
                            @endif
                        @else    
                            <img src="{{ asset('foto/Male.jpg') }}" class="img-fluid" alt="" >
                        @endif
                        
                        @php
                            $link = "http://$_SERVER[HTTP_HOST]/2020/pelatihan/detail_data_code/".$qrcode;
                        @endphp
                        <div class="text-center">
                            <div>
                                {!! QrCode::size(250)->generate($link) !!}
                            </div>
                            <!--
                            <div>
                                <h3> {{ $qrcode }}</h3>
                            </div>
                            -->
                        </div>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th scope="row">NOMOR KTP</th>
                                    <td>{{ $data_code_all[0]['nik'] }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">NAMA LENGKAP</th>
                                    <td>{{ $data_code_all[0]['nama'] }}</td>
                                </tr>

                                <tr>
                                    <th scope="row">TEMPAT, TANGGAL LAHIR</th>
                                    <td>{{ $data_code_all[0]->tempat_lahir }}, {{ strftime("%d %B %Y", strtotime($data_code_all[0]->tanggal_lahir)) }}</td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">NAMA KEGIATAN</th>
                                    <td>{{ $data_code_all[0]->pelatihan[0]->nama_pelatihan }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">PROVINSI KEGIATAN</th>
                                    <td>{{ $data_code_all[0]->pelatihan[0]->provinsi->provinsi }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">TAHUN KEGIATAN</th>
                                    <td>{{ date_format($data_code_all[0]->pelatihan[0]->tanggal_selesai, 'Y') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="col-lg-12 text-center">
                    <br>
                        <img src="{{ asset('foto/VALID.png') }}" alt="" class="img img-responsive" width="300">
                    </div>
                </div>
            </main> 
        </div>
    </div>
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
</body>

</html>