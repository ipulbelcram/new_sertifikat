<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodePelatihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_pelatihan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_code_id')->unsigned();
            $table->string('code_user', 1)->index();
            $table->date('tanggal_input');
            $table->integer('no_urut', 4)->zerofill();
            $table->enum('status', ['Y', 'T']);
            $table->timestamps();
        });

        //Set FK di kolom pelatihan_id di table code_pelatihan
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
