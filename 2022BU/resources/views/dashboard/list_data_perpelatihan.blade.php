@extends('dashboard-template')
@section('content')
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-lg-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                @php
                  foreach($data_pelatihan as $pelatihan){
                    $nama_pelatihan = $pelatihan->nama_pelatihan;
                    $tempat_pealatihan = $pelatihan->tempat;
                    $tanggal_pelatihan = strftime("%d %B %Y", strtotime($pelatihan->tanggal_mulai))." s.d ".strftime("%d %B %Y", strtotime($pelatihan->tanggal_selesai));
                    $kab_kota = $pelatihan->kabupaten_kota->kab_kota;
                    $provinsi_pelatian = $pelatihan->provinsi->provinsi;
                  }
                @endphp
                <h3 class="page-title">List Peserta {{ $nama_pelatihan }}</h3>
                <hr>
                <table>
                  <tr>
                    <th>Tempat Pelaksanaan</th>
                    <td class="px-4">:</td>
                    <td>{{ $tempat_pealatihan }}</td>
                  </tr>
                  <tr>
                    <th>Tanggal Pelaksanaan</th>
                    <td class="px-4">:</td>
                    <td>{{ $tanggal_pelatihan }}</td>
                  </tr>
                  <tr>
                    <th>Kab/ Kota Kegiatan</th>
                    <td class="px-4">:</td>
                    <td>{{ $kab_kota }}</td>
                  </tr>
                  <tr>
                    <th>Provinsi</th>
                    <td class="px-4">:</td>
                    <td>{{ $provinsi_pelatian }}</td>
                  </tr>
                </table>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">List Data</h6>
                  </div>
                  <div class="card-body p-0 pb-3">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">#</th>
                          <th scope="col" class="border-0" style="width:15%">Nama</th>
                          <th scope="col" class="border-0" style="width:13%">Jenis Kelamin</th>
                          <th scope="col" class="border-0">Usia</th>
                          <th scope="col" class="border-0" style="width:14%">No.Telp</th>
                          <th scope="col" class="border-0" style="width:15%">Code</th>
                        </tr>
                      </thead>
                      <tbody>
                      @php 
                          $no = ($data_code->perPage() * $data_code->currentPage()) - ($data_code->perPage() - 1);
                      @endphp
                        @foreach($data_code as $data)
                        <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $data->nama }}</td>
                          <td class="text-center">{{ $data->jenis_kelamin }}</td>
                          <td>{{ $data->usia }} Th</td>
                          <td>{{ !empty($data->no_telephone) ? $data->no_telephone : "-" }}</td>
                          @php
                            if(!empty($data->code_pelatihan->no_urut) && $data->code_pelatihan->status == 'Y') {
                                $tanggal_input = $data->code_pelatihan->tanggal_input;
                                list($tahun, $bulan, $tanggal) = explode('-', $tanggal_input);
                                $QrCode = $data->code_pelatihan->code_user.''.$tahun.''.$bulan.''.$tanggal.''.sprintf("%04d", $data->code_pelatihan->no_urut);
                            } else {
                                $QrCode = "(belum membuat qrcode)";
                            }
                          @endphp
                          <td>{{ $QrCode }}</td>
                          <td>
                        <div class="btn-group">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu">
                                <a href="{{ url('dashboard2/detail_peserta/'.$data->id) }}" class="dropdown-item" target="_blank">Lihat Detail</a>
                            </div>
                        </div>
                    </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <th>Jumlah</th>
                        <th>{{ $data_code->total() }} Peserta</th>
                      </tfoot>
                    </table>
                    <div class="card-footer border-top">
                    <div class="row">
                      <div class="col">
                      {!! $data_code->links() !!}
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- End Small Stats Blocks -->
@stop