<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pelatihan_id')->unsigned();
            $table->string('nik', 16)->unique();
            $table->string('nama', 55);
            $table->string('tempat_lahir', 55);
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->integer('usia');
            $table->integer('pendidikan_terakhir_id')->unsigned()->index();
            $table->integer('agama_id')->unsigned();
            $table->text('alamat');
            $table->string('no_telephone', 16);
            $table->string('email');
            $table->string('asal_lembaga', 55);
            $table->string('rencana_usaha');
            $table->string('kabupaten/kota', 55);
            $table->string('foto', 255)->nullable();
            $table->timestamps();
        });

        Schema::table('data_code', function (Blueprint $table) {
            $table->foreign('pelatihan_id')
                  ->references('id')->on('pelatihan')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });

        Schema::table('code_pelatihan', function (Blueprint $table) {
            $table->foreign('data_code_id')
                  ->references('id')->on('data_code')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_code');
    }
}
