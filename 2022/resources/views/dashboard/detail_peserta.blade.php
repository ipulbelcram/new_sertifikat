@extends('dashboard-template')
@section('content')
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-lg-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Detail Peserta</h3><hr>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              <div class="col-lg-4">
                <div class="card card-small mb-4 pt-3">
                  <div class="card-header border-bottom text-center">
                    <div class="mb-3 mx-auto">
                      <img class="rounded-circle" src="{{ asset('foto/'.$data_peserta[0]->foto) }}" alt="User Avatar" width="110"> 
                    </div>
                    <h4 class="mb-0">{{ $data_peserta[0]->nama }}</h4>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-4">

                        @php 
                          $link = url("/pelatihan/detail_data_code/".$data_peserta[0]->qr_code_no);
                        @endphp
                        <div> {!! QrCode::size(295)->generate($link) !!} </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">DATA PESERTA</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <table class="table table-borderless">
                          <tbody>
                            <tr>
                              <th>Nama Lengkap</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->nama }}</td>
                            </tr>
                            <tr>
                              <th>Nomor KTP</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->nik }}</td>
                            </tr>
                            <tr>
                              <th>Tempat, Tanggal Lahir</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->tempat_lahir }}, {{ strftime("%d %B %Y", strtotime($data_peserta[0]->tanggal_lahir)) }}</td>
                            </tr>
                            <tr>
                              <th>Status</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->status }}</td>
                            </tr>
                            <tr>
                              <th>Jenis Kelamin</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->jenis_kelamin }}</td>
                            </tr>
                            <tr>
                              <th>Agama</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->agama }}</td>
                            </tr>
                            <tr>
                              <th>Pendidikan Terakhir</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->pendidikan }}</td>
                            </tr>
                            <tr>
                              <th>Nomor NPWP</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->npwp }}</td>
                            </tr>
                            <tr>
                              <th>Pekerjaan/ Jabatan</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->pekerjaan_jabatan }}</td>
                            </tr>
                            <tr>
                              <th>Alamat Rumah</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->alamat }}</td>
                            </tr>
                            <tr>
                              <th>Kab/ Kota Peserta</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->kabupaten_kota_peserta }}</td>
                            </tr>
                            <tr>
                              <th>Telp/ Hp</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->no_telephone }}</td>
                            </tr>
                            <tr>
                              <th>Email</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->email }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">DATA KOPERASI/ UMKM PESERTA</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <table class="table table-borderless">
                          <tbody>
                            <tr>
                              <th>Status Usaha</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->status_usaha }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">DATA KOPERASI</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <table class="table table-borderless">
                          <tbody>
                            <tr>
                              <th>Sektor Usaha Koperasi</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->bidang_usaha_koperasi }}</td>
                            </tr>
                            <tr>
                              <th>Nama Koperasi</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->nama_koperasi }}</td>
                            </tr>
                            <tr>
                              <th>Alamat Koperasi</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->alamat_koperasi }}</td>
                            </tr>
                            <tr>
                              <th>Jenis Koperasi</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->bidang_koperasi }}</td>
                            </tr>
                            <tr>
                              <th>No Induk Koperasi</th>
                              <th>:</th>
                              <td>{{ !empty($data_peserta[0]->nik_koperasi) && $data_peserta[0]->nik_koperasi == 'true' ? 'Ada' : 'Tidak Ada' }}</td>
                            </tr>
                            <tr>
                              <th>Jumlah Tenaga Kerja Koperasi</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->tenaga_kerja_koperasi }}</td>
                            </tr>
                            <tr>
                              <th>Omset Koperasi Per Tahun</th>
                              <th>:</th>
                              <td>{{ !empty($data_peserta[0]->omset_koperasi) ? 'Rp. '.number_format($data_peserta[0]->omset_koperasi,0,',','.') : '' }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">DATA UMKM</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <table class="table table-borderless">
                          <tbody>
                            <tr>
                              <th>Sektor Usaha UMKM</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->sektor_usaha }}</td>
                            </tr>
                            <tr>
                              <th>Nama Usaha (UMKM)</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->nama_umkm }}</td>
                            </tr>
                            <tr>
                              <th>Alamat Usaha (UMKM)</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->alamat_umkm }}</td>
                            </tr>
                            <tr>
                              <th>Jenis Usaha (UMKM)</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->jenis_usaha }}</td>
                            </tr>
                            <tr>
                              <th>Bidang Usaha (UMKM)</th>
                              <th>:</th>
                              <td>{{ substr($data_peserta[0]->bidang_usaha, 5) }}</td>
                            </tr>
                            <tr>
                              <th>Lama Usaha (UMKM)</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->lama_usaha }}</td>
                            </tr>
                            <tr>
                              <th>Jumlah Tenaga Kerja UMKM</th>
                              <th>:</th>
                              <td>{{ $data_peserta[0]->tenaga_kerja_umkm }}</td>
                            </tr>
                            <tr>
                              <th>Omset Usaha Per Bulan</th>
                              <th>:</th>
                              <td>{{ !empty($data_peserta[0]->omset_usaha_perbulan) ? 'Rp. '.number_format($data_peserta[0]->omset_usaha_perbulan,0,',','.') : '' }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <!-- End Small Stats Blocks -->
@stop