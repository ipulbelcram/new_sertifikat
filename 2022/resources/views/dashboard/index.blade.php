@extends('dashboard-template')
@section('style')
  <style>
  .stats-small {
    min-height:7.5em !important
  }
  </style>
@endsection
@section('content')
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-12">
                <h4 class="font-weight-bold">KINERJA PELAKSANAAN KEGIATAN PELATIHAN T.A 2022</h4>
                <h4 class="mt-1" style="color: #007bff">Total Peserta Pelatihan : {{ number_format($jumlah_data_code,'0',',','.') }}</h4>
                <a href="{{ url('excel/download_peruser') }}" target="_blank"><button class="btn btn-primary">Format Working Paper All</button></a>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->

            
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Angkatan Pelatihan</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($total_pelatihan,'0',',','.') }} Kegiatan</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Peserta Pelatihan</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($jumlah_data_code,'0',',','.') }} Peserta</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-2"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Koperasi dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($jumlah_koperasi,'0',',','.') }} Koperasi</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-3"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah UMKM dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($jumlah_umkm,'0',',','.') }} UMKM</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-4"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Omset Usaha Koperasi dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">Rp. {{ number_format($omset_usaha_koperasi->omset_koperasi,0,',','.') }}</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-5"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Omset Usaha UMKM dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">Rp. {{ number_format($omset_usaha_umkm->omset_usaha,0,',','.') }}</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-6"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Tenaga Kerja Koperasi dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($tenaga_kerja_koperasi->tenaga_kerja_koperasi,0,',','.') }} Tenaga Kerja</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-7"></canvas>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <a href="#" style="text-decoration:none;">
                  <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                      <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                          <span class="stats-small__label text-uppercase">Jumlah Tenaga kerja UMKM dilatih</span>
                          <h6 class="stats-small__value count my-3" style="font-size:25px;">{{ number_format($tenaga_kerja_umkm->tenaga_kerja_umkm,0,',','.') }} Tenaga Kerja</h6>
                        </div>
                        <!-- <div class="stats-small__data">
                          <span class="stats-small__percentage mt-n2" style="font-size:20px;" ></span>
                        </div> -->
                      </div>
                      <canvas height="120" class="blog-overview-stats-small-8"></canvas>
                    </div>
                  </div>
                </a>
              </div>
            </div>

            
            <!-- End Small Stats Blocks -->
@stop