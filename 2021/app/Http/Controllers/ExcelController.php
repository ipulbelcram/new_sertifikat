<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\WorkingPaper;
use App\Exports\DownloadPerUser;
use Excel;
use Illuminate\Support\Facades\DB;

class ExcelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('notAdminStatistik')->except(['download_peruser']);
    }
    
    public function export($pelatihan_id) 
    {
        return Excel::download(new WorkingPaper($pelatihan_id), 'Format Laporan.xlsx');
    }

    public function download_peruser($user_id = null)
    {
        if(!empty($user_id)) {
            $data =  DB::table('vw_all_data_code')->where('user_id', $user_id)->get();
            return view('excel.workingPaper', compact('data'));
            // return Excel::download(new DownloadPerUser($user_id), 'Format Working Paper.xlsx');
        } else {
            $data =  DB::table('vw_all_data_code')->get();
            return view('excel.workingPaper', [ 'data' => $data ]);
            // return Excel::download(new DownloadPerUser, 'Format Working Paper All.xlsx');
        }
    }
}
