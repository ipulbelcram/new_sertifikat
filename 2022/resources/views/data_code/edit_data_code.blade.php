@extends('template')
@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">DATA CODE PELATIHAN KUKM</h1>
</div>
{!! Form::model($data_code, ['method' => 'PATCH', 'files'=>'true', 'action' => ['DataPelatihanController@update', $data_code->id, $currentPage]]) !!}
    @include('data_code/form_data_code')
{!! Form::close() !!}

@endSection

@section('script')
    <script src="{{ asset('js/dataCodeForm.js') }}"></script>
@endSection

