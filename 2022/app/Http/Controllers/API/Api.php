<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\BidangUsahaResource;
use App\Http\Resources\KabKotaResource;
use App\Params;

class Api extends Controller
{
    public function bidang_usaha(Request $request)
    {
        if(!empty($request->jenis_usaha_id)) {
            $bidang_usaha = Params::where([['category_params', 'bidang_usaha_umkm'], ['active', 'true'], ['pid', $request->jenis_usaha_id]])->orderBy('order', 'ASC')->get();
        } else {
            $bidang_usaha = Params::where([['category_params', 'bidang_usaha_umkm'], ['active', 'true']])->orderBy('order', 'ASC')->get();
        }
        return BidangUsahaResource::collection($bidang_usaha);
    }

    public function kabupaten_kota(Request $request)
    {
        if(!empty($request->provinsi_id)) {
            $kabupaten_kota = DB::table('kab_kota')->where('provinsi_id', $request->provinsi_id)->get();
        } else {
            $kabupaten_kota = DB::table('kab_kota')->get();
        }
        return KabKotaResource::collection($kabupaten_kota);
    }
}
