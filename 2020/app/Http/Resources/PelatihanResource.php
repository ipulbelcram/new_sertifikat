<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PelatihanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'jenis_pelatihan_id' => $this->jenis_pelatihan_id,
            'nama_pelatihan' => $this->nama_pelatihan,
            'tempat' => $this->tempat,
            'tanggal_mulai' => $this->tanggal_mulai,
            'tanggal_selesai' => $this->tanggal_selesai,
            'provinsi_id' => $this->provinsi_id,
            'kabupaten_kota_id' => $this->kabupaten_kota_id,
            'dalam_rangka_id' => $this->dalam_rangka_id,
            'dalam_rangka_des' => $this->dalam_rangka_des,
            'dukungan_sektor_id' => $this->dukungan_sektor_id,
            'metode_pelaksanaan_id' => $this->metode_pelaksanaan_id,
            'penangguung_jawab_swakelola' => $this->penanggung_jawab_swakelola,
            'pelaksana_swakelola' => $this->pelaksana_swakelola,
            'no_hp_pelaksana_swakelola' => $this->no_hp_pelaksana_swakelola,
            'nama_perusahaan' => $this->nama_perusahaan,
            'direktur_utama' => $this->direktur_utama,
            'no_hp' => $this->no_hp,
            'penangguung_jawab_kontraktual' => $this->penanggung_jawab_kontraktual,
            'pelaksana_kontraktual' => $this->pelaksana_kontraktual,
            'no_hp_pelaksana_kontraktual' => $this->no_hp_pelaksana_kontraktual,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ];
    }
}
