<style>
    .active-link {
        background: #007bff !important;
        color: white !important;
    }
</style>

@extends('template')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">PENCARIAN BERDASARKAN NAMA</h1>
</div>
<div>
    <div><h5>Deputi Pengembangan SDM</h5></div>
    <div><h5>Kementerian Koperasi dan UKM Republik Indonesia</h5></div>
</div>
<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Nama</th>
            <th>Nomor KTP</th>
            <th>Nama Kegiatan</th>
            <th>Kab/ Kota Kegiatan</th>
            <th>Tanggal Pelaksanaan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <th>Jumlah</th>
        <th colspan="7">{{ $list_code->total() }} Pencarian</th>        
    </tfoot>
    <tbody>
        @php 
            $no = ($list_code->perPage() * $list_code->currentPage()) - ($list_code->perPage() - 1);
        @endphp
        @foreach($list_code as $code)
        <tr>
            <th style="width:3%;">{{ $no++ }}</th>
            @php
                if(!empty($code->qr_code_no)) {    
                    $qrcode_depan = substr($code->qr_code_no,0,9);
                    $qrcode_belakang = sprintf("%04d", substr($code->qr_code_no,9));
                    $qrcode = $qrcode_depan."".$qrcode_belakang;
                } else {
                    $qrcode = '-';
                }
                
            @endphp
            <th>{{ $qrcode }}</th>
            <th style="width:14%;">{{ $code->nama }}</th>
            <th style="width:14%;">{{ $code->nik }}</th>
            <th style="width:20%;">{{ $code->nama_pelatihan }}</th>
            <th>{{ $code->kabupaten_kota }}</th>
            <th style="width:16%;">{{ strftime("%d %B %Y", strtotime($code->tanggal_mulai)) }} s/d {{ strftime("%d %B %Y", strtotime($code->tanggal_selesai)) }}</th>
            <td>
                <div class="btn-group">
                    <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu">
                        @if($code->status_pelatihan != 'Y' OR Auth::user()->code_user == "Z")
                            <a href="{{ url('pelatihan/edit_data_code/'.$code->data_code_id.'/1') }}" class="dropdown-item">Edit</a>
                        @endif
                        <a href="{{ url('pelatihan/detail_data_code/'.$code->qr_code_no) }}" class="dropdown-item" target="_blank">Lihat QrCode</a>
                        @if(empty($code->qr_code_no) && Auth::user()->code_user == 'Z')
                            <a href="{{ url('pelatihan/updateQrcode/'.$code->data_code_id.'/1') }}" class="dropdown-item">Buat QrCode</a>
                        @endif

                        @if(!empty($code->qr_code_no))
                            <a href="{{ url('pelatihan/print_code/'.$code->data_code_id.'/Y') }}" class="dropdown-item" target="_blank">Print Dengan Tanggal</a>
                            <a href="{{ url('pelatihan/print_code/'.$code->data_code_id.'/N') }}" class="dropdown-item" target="_blank">Print Tanpa Tanggal</a>
                            <a href="{{ url('pelatihan/print_code/'.$code->data_code_id.'/F') }}" class="dropdown-item" target="_blank">Print Dengan Foto</a>
                            <a href="{{ route('data_pelatihan.download_pdf', ['data_code_id' => $code->data_code_id, 'status' => 'Y']) }}" class="dropdown-item">Sertifikat PDF (Background)</a>
                            <a href="{{ route('data_pelatihan.download_pdf', ['data_code_id' => $code->data_code_id, 'status' => 'N']) }}" class="dropdown-item">Sertifikat PDF (No Background)</a>
                        @endif

                        @if($code->status_pelatihan != 'Y' OR Auth::user()->code_user == "Z")
                            <a href="{{ url('pelatihan/delete_data_code/'.$code->pelatihan_id.'/'.$code->data_code_id.'/1') }}" class="dropdown-item" onClick="return confirm('Apa Kamu Yakin Ingin Menghapusnya ?')">Delete</a>
                        @endif
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="float-right">
    {{ $list_code->links() }}
</div>
@stop