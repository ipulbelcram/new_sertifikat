<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DataCode;
use App\CodePelatihan;
use App\Pelatihan;
use App\Agama;
use App\Pendidikan;
use App\PekerjaanJabatan;
use App\BidangKoperasi;
use App\BidangUsaha;
use App\Params;
use Storage;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\DataCodeRequest;

class DataPelatihanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [ 'except' => [
            'detail',
        ]]);
        $this->middleware('notAdminStatistik', ['except' => [
            'detail',
        ]]);
    }
    
    public function index($id_pelatihan)
    {
        $data_pelatihan = Pelatihan::all()->where('id', $id_pelatihan);
        $ket_pelatihan = DB::table('vw_ket_pelatihan')->select('jumlah_peserta', 'status_Y')->where('pelatihan_id', $id_pelatihan)->get();
        $data_code = DataCode::where('pelatihan_id', $id_pelatihan)->orderby('id', 'desc')->paginate(10);
        $jumlah_data_code = DataCode::where('pelatihan_id', $id_pelatihan)->count();
        return view('data_code/index', compact('data_pelatihan', 'data_code', 'jumlah_data_code','ket_pelatihan'));
    }

    public function create($id_pelatihan)
    {
        $code_pelatihan_terakhir = CodePelatihan::all('no_urut')->last();   

        $list_agama = Agama::pluck('agama', 'id');
        $list_pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $list_pekerjaan_jabatan = PekerjaanJabatan::orderBy('order', 'ASC')->pluck('pekerjaan_jabatan', 'id');
        // $list_bidang_usaha = BidangUsaha::pluck('bidang_usaha', 'id');
        // $list_bidang_koperasi = BidangKoperasi::pluck('bidang_koperasi', 'id');
        $list_bidang_usaha = Params::where([['category_params', 'bidang_usaha'], ['active', 'true']])->pluck('params', 'id');
        $list_bidang_koperasi = Params::where([['category_params', 'jenis_koperasi'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        $status_usaha = Params::where([['category_params', 'status_usaha'], ['active', 'true']])->pluck('params', 'id');
        $bidang_usaha_koperasi = Params::where([['category_params', 'bidang_usaha_koperasi'], ['active', 'true']])->pluck('params', 'id');
        $jenis_usaha = Params::where([['category_params', 'jenis_umkm'], ['active', 'true']])->pluck('params', 'id');

        $data_pelatihan = Pelatihan::findOrfail($id_pelatihan);
        $pelatihan_id = $data_pelatihan->id;
        $button = "Create";
        return view('data_code/data_code', compact('pelatihan_id', 'list_agama', 'list_pendidikan', 'list_pekerjaan_jabatan', 'list_bidang_usaha', 'list_bidang_koperasi', 'button', 'code_pelatihan_terakhir', 'status_usaha', 'bidang_usaha_koperasi', 'jenis_usaha'));
    }

    public function store( DataCodeRequest $request) 
    {
        $pelatihan_id = $request->pelatihan_id;
        $nik_peserta = $request->nik;
        $count_data_code = DataCode::where(['pelatihan_id' => $pelatihan_id, 'nik' => $nik_peserta])->count();
        if($count_data_code >= 1 ) {
            echo "<script> 
                var id_pelatihan = $pelatihan_id;
                alert('NIK peseta di pelatihan ini sudah ada !!!');
                history.go(-1);
            </script>";
            
        } else {
            $input = $request->all();
            $input = $this->validasi_status_usaha($request);
            // if(!empty($request->omset_koperasi)) {
            //     $input['omset_koperasi'] = $this->number($request->omset_koperasi);
            // }

            // if(!empty($request->omset_usaha_perbulan)) {
            //     $input['omset_usaha_perbulan'] = $this->number($request->omset_usaha_perbulan);
            // }

            // if(!empty($request->tenaga_kerja_umkm)) {
            //     $input['tenaga_kerja_umkm'] = $this->number($request->tenaga_kerja_umkm);
            // }

            // if(!empty($request->tenaga_kerja_koperasi)) {
            //     $input['tenaga_kerja_koperasi'] = $this->number($request->tenaga_kerja_koperasi);
            // }

            if(!empty($request->image_name)) {
                $foto_name = $request->image_name;
                Storage::move('foto_coba/'.$foto_name, 'foto/'.$foto_name);
                $input['foto'] = $foto_name;
            }

            $tanggal_lahir = Carbon::parse($request->input('tanggal_lahir'));
            $input['usia'] = $tanggal_lahir->age;
            $data_code = DataCode::create($input);

            $id_pelatihan = $request->pelatihan_id;
            return redirect()->route('data_peserta_pelatihan', [$id_pelatihan]);
        }
    }

    public function image_upload(Request $request) {
        $data = $request->image;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name = date('YmdHis').".png";
        $path = public_path() . "/foto_coba/" . $image_name;
        file_put_contents($path, $data);
        echo"<input name='image_name' type='text' value='$image_name' hidden>";
        //return response()->json(['success'=>'done']);
        
    }

    public function edit($data_code_id, $currentpage)
    {   
        $data_code = DataCode::findOrFail($data_code_id);

        $list_agama = Agama::pluck('agama', 'id');
        $list_pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $list_pekerjaan_jabatan = PekerjaanJabatan::orderBy('order', 'ASC')->pluck('pekerjaan_jabatan', 'id');
        // $list_bidang_usaha = BidangUsaha::pluck('bidang_usaha', 'id');
        // $list_bidang_koperasi = BidangKoperasi::pluck('bidang_koperasi', 'id');
        $list_bidang_usaha = Params::where([['category_params', 'bidang_usaha'], ['active', 'true']])->pluck('params', 'id');
        $list_bidang_koperasi = Params::where([['category_params', 'jenis_koperasi'], ['active', 'true']])->orderBy('order','ASC')->pluck('params', 'id');
        $status_usaha = Params::where([['category_params', 'status_usaha'], ['active', 'true']])->pluck('params', 'id');
        $bidang_usaha_koperasi = Params::where([['category_params', 'bidang_usaha_koperasi'], ['active', 'true']])->pluck('params', 'id');
        $jenis_usaha = Params::where([['category_params', 'jenis_umkm'], ['active', 'true']])->pluck('params', 'id');

        $pelatihan_id = $data_code->pelatihan_id;
        $button = "Update";
        $currentPage = $currentpage;
        return view('data_code/edit_data_code', compact('data_code', 'pelatihan_id', 'list_agama', 'list_pekerjaan_jabatan', 'list_pendidikan', 'list_bidang_usaha', 'list_bidang_koperasi', 'button', 'currentPage', 'status_usaha', 'bidang_usaha_koperasi', 'jenis_usaha'));
    }
    
    public function update($data_code_id, $currentpage, DataCodeRequest $request)
    {
        $input = $request->all();
        $data_code = DataCode::findOrFail($data_code_id);

        $input = $this->validasi_status_usaha($request);
        // if(!empty($request->omset_koperasi)) {
        //     $input['omset_koperasi'] = $this->number($request->omset_koperasi);
        // }

        // if(!empty($request->omset_usaha_perbulan)) {
        //     $input['omset_usaha_perbulan'] = $this->number($request->omset_usaha_perbulan);
        // }

        // if(!empty($request->tenaga_kerja_umkm)) {
        //     $input['tenaga_kerja_umkm'] = $this->number($request->tenaga_kerja_umkm);
        // }

        // if(!empty($request->tenaga_kerja_koperasi)) {
        //     $input['tenaga_kerja_koperasi'] = $this->number($request->tenaga_kerja_koperasi);
        // }
        
        if($request->hasFile('foto')){
            $exist = Storage::disk('foto')->exists($data_code->foto);
            if(isset($data_code->foto) && $exist ){
                Storage::disk('foto')->delete($data_code->foto);
            }

            if(!empty($request->image_name)) {
                $foto_name = $request->image_name;
                Storage::move('foto_coba/'.$foto_name, 'foto/'.$foto_name);
                $input['foto'] = $foto_name;
            }
        }
        $tanggal_lahir = Carbon::parse($request->input('tanggal_lahir'));
        $input['usia'] = $tanggal_lahir->age;
        $data_code->update($input);
        $pelatihan_id = $request->pelatihan_id;
        return redirect()->route('data_peserta_pelatihan',[$pelatihan_id, 'page' => $currentpage]);
    }

    public function detail($QrCode) {
        $code_user = substr($QrCode, 0,1);
        $tanggal_input = substr($QrCode,1,4).'-'.substr($QrCode,5,2).'-'.substr($QrCode,7,2);
        $no_urut = substr($QrCode, 9);
        $qrcode = $QrCode;
        $data_code_pelatihan = CodePelatihan::where(['code_user' => $code_user, 'tanggal_input' => $tanggal_input, 'no_urut' => $no_urut, 'status' => 'Y',])->pluck('data_code_id');
        if($data_code_pelatihan->count() >= 1) {
           foreach($data_code_pelatihan as $data_code_id){
               $data_code_all  = DataCode::where('id', $data_code_id)->get();
            }
            //echo var_dump($data_code_all->pelatihan);
            return view('data_code/detail_data_code', compact('qrcode', 'data_code_all'));
        } else {
            echo "<script>alert('QrCode Belum Di Buat');window.close();</script>";
        }
    }

    public function print($code_pelatihan_id)
    {
        $code_pelatihan = DataCode::findOrFail($code_pelatihan_id);
        $code = CodePelatihan::where('code_pelatihan', $code_pelatihan_id)->get();
        return view('data_code/print', compact('code_pelatihan', 'code'));
    }

    public function destroy($pelatihan_id, $data_code_id, $currentpage)
    {
        $data_code = DataCode::findorFail($data_code_id);
        $this->deleteImage('foto', $data_code->foto);
        $delete = DB::select("call DeleteDataCode('".$data_code_id."')");
        return redirect()->route('data_peserta_pelatihan', [$pelatihan_id, 'page' => $currentpage]);
    }

    public function validasi_foto($pelatihan_id)
    {   
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $data_pelatihan = Pelatihan::all()->where('id', $pelatihan_id);
        $data_code = $data_code = DataCode::where('pelatihan_id', $pelatihan_id)->orderby('id', 'desc')->paginate(10);
        return view('data_code/validasi_foto', compact('pelatihan', 'data_code'));
    }

    /*public function search_nik(Request $request)
    {
        $kata_kunci = $request->kata_kunci;
        $code_user = Auth::user()->code_user;

        if($code_user == "Z") {
            $nama_peserta = DataCode::where('nik', $kata_kunci)->first();
            $list_data = DB::table('vw_all_data_code')->where('nik', $kata_kunci)->get();
        } else {
            $nama_peserta = DB::table('vw_all_data_code')->where([
                ['nik','=', $kata_kunci],
                ['code_user','=', $code_user]
            ])->first();
            $list_data = DB::table('vw_all_data_code')->where([
                ['nik','=', $kata_kunci],
                ['code_user','=', $code_user]
            ])->get();
        }
        return view('data_code/search', compact('nama_peserta', 'list_data'));

    }*/

    public function search_nik(Request $request)
    {
        $kata_kunci = $request->kata_kunci;
        $nama_peserta = DataCode::where('nik', $kata_kunci)->first();
        $list_data = DB::table('vw_all_data_code')->where('nik', $kata_kunci)->get();
        return view('data_code/search', compact('nama_peserta', 'list_data'));
    }

    public function deleteImage($patch, $foto_name) 
    {
        $exist = Storage::disk('local_public')->exists($patch.'/'.$foto_name);
        if(isset($foto_name)) {
            Storage::disk('local_public')->delete($patch.'/'.$foto_name);
        }
    }

    public function download_pdf($data_code_id, $status = NULL)
    {
        $data_code = DB::table('vw_all_data_code')->where('data_code_id', $data_code_id)->first();
        // $pdf = PDF::loadView('data_code.data_code_pdf', compact('data_code', 'status'))->setPaper('a4')->setOrientation('landscape');
        // $pdf->setOption('margin-top',0);
        // $pdf->setOption('margin-bottom',0);
        // $pdf->setOption('margin-left',0);
        // $pdf->setOption('margin-right',0);

        // return $pdf->download($data_code->nik.'_'.$data_code->nama.'.pdf');
        return view('data_code.data_code_pdf', compact('data_code', 'status'));
    }

    private function number($number) 
    {
        $string_number = preg_replace('/[^0-9 &%\']/', '', $number);
        $input_number = trim($string_number);
        return $input_number;
    }

    private function validasi_status_usaha($request)
    {
        $input = $request->all();

        if($request->status_usaha_id == 25) {
            //KOPERASI REQUIRED
            $input['bidang_usaha_koperasi_id'] = $request->bidang_usaha_koperasi_id;
            $input['asal_lembaga'] = $request->asal_lembaga;
            $input['alamat_koperasi'] = $request->alamat_koperasi;
            $input['nik_koperasi'] = $request->nik_koperasi;
            $input['bidang_koperasi_id'] = $request->bidang_koperasi_id;
            $input['tenaga_kerja_koperasi'] = $this->number($request->tenaga_kerja_koperasi);
            $input['omset_koperasi'] = $this->number($request->omset_koperasi);

            //USAHA NULLABLE
            $input['rencana_usaha'] = NULL;
            $input['nama_umkm'] = NULL;
            $input['alamat_umkm'] = NULL;
            $input['jenis_usaha_id']  = NULL;
            $input['bidang_usaha_id'] = NULL;
            $input['lama_usaha'] = NULL;
            $input['tenaga_kerja_umkm'] = NULL;
            $input['omset_usaha_perbulan'] = NULL;

        } else if($request->status_usaha_id == 26) {
            //KOPERASI NULLABLE
            $input['bidang_usaha_koperasi_id'] = NULL;
            $input['asal_lembaga'] = NULL;
            $input['alamat_koperasi'] = NULL;
            $input['nik_koperasi'] = NULL;
            $input['bidang_koperasi_id'] = NULL;
            $input['tenaga_kerja_koperasi'] = NULL;
            $input['omset_koperasi'] = NULL;

            //USAHA REQUIRED
            $input['rencana_usaha'] = $request->rencana_usaha;
            $input['nama_umkm'] = $request->nama_umkm;
            $input['alamat_umkm'] = $request->alamat_umkm;
            $input['jenis_usaha_id']  = $request->jenis_usaha_id;
            $input['bidang_usaha_id'] = $request->bidang_usaha_id;
            $input['lama_usaha'] = $request->lama_usaha;
            $input['tenaga_kerja_umkm'] = $this->number($request->tenaga_kerja_umkm);
            $input['omset_usaha_perbulan'] = $this->number($request->omset_usaha_perbulan);

        } else {
            //KOPERASI NULLABLE
            $input['bidang_usaha_koperasi_id'] = NULL;
            $input['asal_lembaga'] = NULL;
            $input['alamat_koperasi'] = NULL;
            $input['nik_koperasi'] = NULL;
            $input['bidang_koperasi_id'] = NULL;
            $input['tenaga_kerja_koperasi'] = NULL;
            $input['omset_koperasi'] = NULL;

            //USAHA NULLABLE
            $input['rencana_usaha'] = NULL;
            $input['nama_umkm'] = NULL;
            $input['alamat_umkm'] = NULL;
            $input['jenis_usaha_id']  = NULL;
            $input['bidang_usaha_id'] = NULL;
            $input['lama_usaha'] = NULL;
            $input['tenaga_kerja_umkm'] = NULL;
            $input['omset_usaha_perbulan'] = NULL;
        }

        return $input;

    }

}
