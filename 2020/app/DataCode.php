<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataCode extends Model
{
    
    protected $table = 'data_code';

    protected $fillable = [
        'pelatihan_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'usia',
        'jenis_kelamin',
        'status',
        'pendidikan_terakhir_id',
        'agama_id',
        'npwp',
        'pekerjaan_jabatan',
        'alamat',
        'no_telephone',
        'email',
        'asal_lembaga',
        'nama_umkm',
        'jenis_usaha_id',
        'bidang_usaha_id',
        'badan_usaha',
        'rencana_usaha',
        'bidang_usaha_koperasi_id',
        'kabupaten_kota',
        'status_usaha_id',
        'alamat_koperasi',
        'alamat_umkm',
        'nik_koperasi',
        'bidang_koperasi_id',
        'lama_usaha', 
        'lama_usaha_koperasi',
        'omset_usaha_perbulan',
        'omset_koperasi',
        'tenaga_kerja_umkm',
        'tenaga_kerja_koperasi',
        'foto',
    ];

    protected $dates = ['tanggal_lahir'];

    public function agama()
    {
        return $this->belongsTo('App\Agama', 'agama_id');
    }

    public function pendidikan()
    {
        return $this->belongsTo('App\Pendidikan', 'pendidikan_terakhir_id');
    }

    public function code_pelatihan()
    {
        return $this->hasOne('App\CodePelatihan', 'data_code_id');
    }

    public function pelatihan()
    {
        return $this->hasMany('App\Pelatihan', 'id','pelatihan_id');
    }

    public function pelatihan2()
    {
        return $this->belongsTo('App\Pelatihan', 'pelatihan_id');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\PekerjaanJabatan', 'pekerjaan_jabatan');
    }
}
