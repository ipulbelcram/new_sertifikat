<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTtdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ttds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_user', 1);
            $table->string('unit');
            $table->string('name');
            $table->string('ttd_path');
        });

        Schema::table('ttds', function (Blueprint $table) {
            $table->foreign('code_user')->references('code_user')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ttds');
    }
}
