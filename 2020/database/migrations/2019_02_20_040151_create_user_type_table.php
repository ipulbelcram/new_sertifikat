<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_type', 225);
            $table->string('code_user', 1)->unique();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('code_user')
                  ->references('code_user')->on('user_type')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });

        Schema::table('code_pelatihan', function (Blueprint $table) {
            $table->foreign('code_user')
                  ->references('code_user')->on('user_type')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type');
    }
}
