<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class QrcodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('notAdminStatistik');
    }
    
    public function index()
    {
        $qrcode = new BaconQrCodeGenerator;
        $code = $qrcode->format('png')->size(500)->generate('Make Qrcode With Laravel');
        return view('qrcode.index', compact('code'));
    }
}
