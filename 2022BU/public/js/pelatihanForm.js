// FORM PROVINSI DAN KABUPATEN KOTA
$(document).ready(function(){
	$("#kabupaten_kota_id option:first").attr('disabled', true);

	var provinsi_id = $("#provinsi_id").val();
	if(provinsi_id == '') {
		$("#kabupaten_kota_id").attr('disabled', true);
	} else {
		$.ajax({
			url:"https://diklat.kemenkopukm.go.id/2020/api/kabupaten_kota",
			type:"GET",
			dataType:"JSON",
			data: {
				provinsi_id : provinsi_id,
			},
			success: function(result) {
				var kabupaten_kota_id = $("#kabupaten_kota_value").val();
				var data = result.data
				$.each(data, function(key, value) {
					$("#kabupaten_kota_id").append("<option value="+ value.id +">"+value.kabupaten_kota+"</<option>")
				})
				$("#kabupaten_kota_id option[value="+kabupaten_kota_id+"]").attr('selected', true);
			}
		})
	}

	$("#provinsi_id").on('change', function() {
		$("#kabupaten_kota_id").html('');
		var provinsi_id = $(this).val();
		$.ajax({
			url:"https://diklat.kemenkopukm.go.id/2020/api/kabupaten_kota",
			type:"GET",
			dataType:"JSON",
			data: {
				provinsi_id : provinsi_id,
			},
			success: function(result) {
				var data = result.data
				$.each(data, function(key, value) {
					$("#kabupaten_kota_id").append("<option value="+ value.id +">"+value.kabupaten_kota+"</<option>")
				})
				$("#kabupaten_kota_id").attr('disabled', false)
			}
		})
	})
})

// FORM METODE PELAKSANAAN
$(document).ready(function() {
	$("#metode_pelaksanaan option:first").attr('disabled', true)

	var metode_pelaksanaan_id = $("#metode_pelaksanaan").val()

	if(metode_pelaksanaan_id == 153) {
		$("#swakelola").show()
		$("#kontraktual").hide()
	} else if (metode_pelaksanaan_id == 154) {
		$("#swakelola").hide()
		$("#kontraktual").show()
	} else {
		$("#swakelola").hide()
		$("#kontraktual").hide()
	}

	$("#metode_pelaksanaan").on('change', function() {
		var metode_pelaksanaan_id = $(this).val();
		if(metode_pelaksanaan_id == 153) {
			$("#swakelola").show()
			$("#kontraktual").hide()
		} else if (metode_pelaksanaan_id == 154) {
			$("#swakelola").hide()
			$("#kontraktual").show()
		} else {
			$("#swakelola").hide()
			$("#kontraktual").hide()
		}	
	})
})