@foreach($data_code_pelatihan as $code_pelatihan)
    <div class="text-center">
    @php
        $link = "http://$_SERVER[HTTP_HOST]/pelatihan/detail_data_code/".$code_pelatihan->code_pelatihan;
    @endphp
        <div>
            {!! QrCode::size(350)->generate($link) !!}
        </div>
        <div >
            <h2>KODE : {{ $code_pelatihan->code_pelatihan }}</h2>
        </div>
    </div>
@endforeach
