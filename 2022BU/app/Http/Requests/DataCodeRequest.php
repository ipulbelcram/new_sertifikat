<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH'){
            // $foto = 'sometimes|image|max:3000|mimes:jpeg,jpg,png';
            $foto = 'sometimes|max:3000|mimes:jpeg,jpg,png';
        } else {
            // $foto = 'required|image|max:3000|mimes:jpeg,jpg,png';
            $foto = 'required|max:3000|mimes:jpeg,jpg,png';
        }

        if($this->status_usaha_id == 25) {
            //KOPERASI REQUIRED
            $bidang_usaha_koperasi_id = 'required|numeric|exists:params,id';
            $asal_lembaga = 'required|string';
            $alamat_koperasi = 'required|string';
            $nik_koperasi = 'required|in:true,false';
            $bidang_koperasi_id = 'required|numeric|exists:params,id';
            $tenaga_kerja_koperasi = 'required|string';
            $omset_koperasi = 'required|string';

            //USAHA NULLABLE
            $rencana_usaha = 'nullable|string';
            $nama_umkm = 'nullable|string';
            $alamat_umkm = 'nullable|string';
            $jenis_usaha_id = 'nullable|numeric|exists:params,id';
            $bidang_usaha_id = 'nullable|numeric|exists:params,id';
            $lama_usaha = 'nullable|string';
            $tenaga_kerja_umkm = 'nullable|string';
            $omset_usaha_perbulan = 'nullable|string';
        } else if($this->status_usaha_id == 26) {
            //KOPERASI NULLABLE
            $bidang_usaha_koperasi_id = 'nullable|numeric|exists:params,id';
            $asal_lembaga = 'nullable|string';
            $alamat_koperasi = 'nullable|string';
            $nik_koperasi = 'nullable|in:true,false';
            $bidang_koperasi_id = 'nullable|numeric|exists:params,id';
            $tenaga_kerja_koperasi = 'nullable|string';
            $omset_koperasi = 'nullable|string';

            //USAHA REQUIRED
            $rencana_usaha = 'required|string';
            $nama_umkm = 'required|string';
            $alamat_umkm = 'required|string';
            $jenis_usaha_id = 'required|numeric|exists:params,id';
            $bidang_usaha_id = 'required|numeric|exists:params,id';
            $lama_usaha = 'required|string';
            $tenaga_kerja_umkm = 'required|string';
            $omset_usaha_perbulan = 'required|string';
        } else {
            //KOPERASI NULLABLE
            $bidang_usaha_koperasi_id = 'nullable|numeric|exists:params,id';
            $asal_lembaga = 'nullable|string';
            $alamat_koperasi = 'nullable|string';
            $nik_koperasi = 'nullable|in:true,false';
            $bidang_koperasi_id = 'nullable|numeric|exists:params,id';
            $tenaga_kerja_koperasi = 'nullable|string';
            $omset_koperasi = 'nullable|string';

            //USAHA REQUIRED
            $rencana_usaha = 'nullable|string';
            $nama_umkm = 'nullable|string';
            $alamat_umkm = 'nullable|string';
            $jenis_usaha_id = 'nullable|numeric|exists:params,id';
            $bidang_usaha_id = 'nullable|numeric|exists:params,id';
            $lama_usaha = 'nullable|string';
            $tenaga_kerja_umkm = 'nullable|string';
            $omset_usaha_perbulan = 'nullable|string';
        }

        return [
            'pelatihan_id' => 'required',
            'nik' => 'required|string|size:16',
            'nama' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'status' => 'required|in:L,M',
            'pendidikan_terakhir_id' => 'required|string',
            'npwp' => 'nullable|string',
            'pekerjaan_jabatan' => 'required|string',
            'agama_id' => 'required',
            'email' => 'nullable|string',
            'no_telephone' => 'required|numeric|digits_between:10,15',
            'alamat' => 'required|string',
            'badan_usaha' => 'nullable|in:K,U',
            'kabupaten_kota' => 'nullable|string',
            'status_usaha_id' => 'required',

            // USAHA (UMKM)
            'rencana_usaha' => $rencana_usaha, // SEKTOR USAHA
            'nama_umkm' => $nama_umkm,
            'alamat_umkm' => $alamat_umkm,
            'jenis_usaha_id' => $jenis_usaha_id,
            'bidang_usaha_id' => $bidang_usaha_id,
            'lama_usaha' => $lama_usaha,
            'tenaga_kerja_umkm' => $tenaga_kerja_umkm,
            'omset_usaha_perbulan' => $omset_usaha_perbulan,

            //KOPERASI
            'bidang_usaha_koperasi_id' => $bidang_usaha_koperasi_id,
            'asal_lembaga' => $asal_lembaga,
            'alamat_koperasi' => $alamat_koperasi,
            'nik_koperasi' => $nik_koperasi,
            'bidang_koperasi_id' => $bidang_koperasi_id,
            'tenaga_kerja_koperasi' => $tenaga_kerja_koperasi,
            'omset_koperasi' => $omset_koperasi,

            'lama_usaha_koperasi' => 'nullable|string',
            'foto' => $foto,
        ];
    }
}
