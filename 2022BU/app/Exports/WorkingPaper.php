<?php

namespace App\Exports;

use App\DataCode;
use App\Pelatihan;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class WorkingPaper implements FromView, ShouldAutoSize, WithEvents
{
    public function __construct(string $pelatihan_id)
    {
        $this->pelatihan_id = $pelatihan_id;
    }
       
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:N1'; // All headers
                $cellRange2 = 'A2:N2'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
            },
        ];
    }

    public function view(): View
    {
        $data = DB::table('vw_all_data_code')->where('pelatihan_id', $this->pelatihan_id)->get();
        $pelatihan = Pelatihan::where('id', $this->pelatihan_id)->first();
        $jumlah_data = DataCode::all()->where('pelatihan_id', $this->pelatihan_id)->count();
        $data_user = User::find($pelatihan->user_id);
        return view('excel.download_laporan', [
            // 'data' => DataCode::all()->where('pelatihan_id', $this->pelatihan_id),
            'data' => $data,
            'pelatihan' => $pelatihan,
            'jumlah_data' => $jumlah_data,
            'data_user' => $data_user
        ]);
    }
}