@extends('dashboard-template')
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-8">
            <h4 class="font-weight-bold">PROFIL PESERTA PELATIHAN</h4>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <!-- JENIS KELAMIN -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">A. Jenis Kelamin</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Jenis Kelamin</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_peserta_jenis_kel = 0;
                                $persentase_jenis_kel = 0;
                            ?>
                            @foreach($jenis_kelamin as $jenis_kelamin)
                            <?php
                                $total_peserta_jenis_kel += $jenis_kelamin->jumlah_jenis_kelamin;
                                $persentase_jenis_kel += $jenis_kelamin->persentase;
                            ?>
                            <tr>
                                <td>{{ $jenis_kelamin->jenis_kelamin }}</td>    
                                <td class="text-center">{{ $jenis_kelamin->jumlah_jenis_kelamin }}</td>
                                <td class="text-center">{{ $jenis_kelamin->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ $total_peserta_jenis_kel }}</td>
                                <td>{{ round($persentase_jenis_kel) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- STATUS -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">B. Status</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Status</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_peserta_status = 0;
                                $persentase_status = 0;
                            ?>
                            @foreach($status as $status)
                            <?php
                                $total_peserta_status += $status->jumlah_data_status;
                                $persentase_status += $status->persentase;
                            ?>
                            <tr>
                                <td>{{ $status->status }}</td>    
                                <td class="text-center">{{ $status->jumlah_data_status }}</td>
                                <td class="text-center">{{ $status->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ round($persentase_status) }}</td>
                                <td>{{ round($persentase_status) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <!-- AGAMA -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">C. Agama</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Agama</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_peserta_agama = 0;
                                $persentase_agama = 0;
                            ?>
                            @foreach($agama as $agama)
                            <?php
                                $total_peserta_agama += $agama->jumlah_data_agama;
                                $persentase_agama += $agama->persentase;
                            ?>
                            <tr>
                                <td>{{ $agama->agama }}</td>    
                                <td class="text-center">{{ $agama->jumlah_data_agama }}</td>
                                <td class="text-center">{{ $agama->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ $total_peserta_agama }}</td>
                                <td>{{ round($persentase_agama) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- PENDIDIKAN TERAKHIR -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">D. Pendidikan Terakhir</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Tingkat Pendidikan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_pserta_pendidikan = 0;
                                $persentase_pendididkan = 0;
                            ?>
                            @foreach($pendidikan as $pendidikan)
                            <?php
                                $total_pserta_pendidikan += $pendidikan->jumlah_pendidikan;
                                $persentase_pendididkan += $pendidikan->persentase;
                            ?>
                            <tr>
                                <td>{{ $pendidikan->pendidikan }}</td>    
                                <td class="text-center">{{ $pendidikan->jumlah_pendidikan }}</td>
                                <td class="text-center">{{ $pendidikan->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ $total_pserta_pendidikan }}</td>
                                <td>{{ round($persentase_pendididkan) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <!-- PEKERJAAN/JABATAN -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">E. Pekerjaan Jabatan</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Pekerjaan/ Jabatan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_peserta_pekerjaan = 0; 
                                $persentase_pekerjaan = 0;
                            ?>
                            @foreach($pekerjaan_jabatan as $jabatan)
                            <?php 
                                $total_peserta_pekerjaan += $jabatan->jumlah_pekerjaan_jabatan ; 
                                $persentase_pekerjaan += $jabatan->persentase;
                            ?> 
                            <tr>
                                <td>{{ $jabatan->pekerjaan_jabatan }}</td>    
                                <td class="text-center">{{ $jabatan->jumlah_pekerjaan_jabatan }}</td>
                                <td class="text-center">{{ $jabatan->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ $total_peserta_pekerjaan }}</td>
                                <td>{{ round($persentase_pekerjaan) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">F. Rentang Usia Peserta Pelatihan</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Rentang Usia</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Usia Belum Produktif (0-14 Tahun)</td>    
                                <td class="text-center">{{ $usia014 }}</td>
                                <td class="text-center">{{ ($usia014 != 0) ? number_format(($usia014/$total_peserta)*100,2) : 0 }} %</td>
                            </tr>
                            <tr>
                                <td>Usia Produktif (15-64 Tahun)</td>    
                                <td class="text-center">{{ $usia1564 }}</td>
                                <td class="text-center">{{ ($usia1564 != 0) ? number_format(($usia1564/$total_peserta)*100,2) : 0}} %</td>
                            </tr>
                            <tr>
                                <td>Usia Sudah Tidak Produktif (65+ Tahun)</td>    
                                <td class="text-center">{{ $usia65 }}</td>
                                <td class="text-center">{{ ($usia65 != 0) ? number_format(($usia65/$total_peserta)*100,2) : 0 }} %</td>
                            </tr>
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ $usia014+$usia1564+$usia65 }}</td>
                                <td>100 %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection