<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodePelatihan extends Model
{
    protected $table = 'code_pelatihan';
    protected $fillable = [
        'data_code_id',
        'code_user',
        'tanggal_input',
        'no_urut',
    ];

    public function data_code()
    {
        return $this->belongsTo('App\DataCode', 'data_code_id');
    }
}
