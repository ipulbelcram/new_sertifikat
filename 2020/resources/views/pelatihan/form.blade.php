    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">FORM PELATIHAN</h1>
    </div>
    <div class="card mb-3">
        <div class="card-header">
            <strong>FORM KEGIATAN</strong>
        </div>
        <div class="card-body">
            {!! Form::hidden('user_id', !empty($data_pelatihan->user_id) ? null : Auth::user()->id) !!}
            <div class="form-row">
                <div class="col-lg-12 mb-3">
                    <label class="font-weight-bold">Jenis Kegiatan</label>
                    {!! Form::select('jenis_pelatihan_id', $jenis_pelatihan, null, ['class' => 'form-control', 'placeholder' => 'Jenis Kegiatan']) !!}
                    <div class="text-danger">
                        @if($errors->has('jenis_pelatihan_id'))
                            {{ $errors->first('jenis_pelatihan_id') }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12 mb-3">
                    <label class="font-weight-bold">Nama Kegiatan</label>
                    {!! Form::text('nama_pelatihan', null, ['class' => 'form-control', 'placeholder' => 'Nama Kegiatan']) !!}
                    <div class="text-danger">
                        @if($errors->has('nama_pelatihan'))
                            {{ $errors->first('nama_pelatihan') }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label class="font-weight-bold">Provinsi</label>
                    @if(count($list_provinsi) > 0)
                        {!! Form::select('provinsi_id', $list_provinsi, null, ['class' => 'form-control', 'id' => 'provinsi_id', 'placeholder' => 'Pilih Provinsi']) !!}
                    @else
                        <p>Tidak Ada Pilihan Provinsi</p>
                    @endif
                    <div class="text-danger">
                        @if($errors->has('provinsi_id'))
                            {{ $errors->first('provinsi_id') }}
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="font-weight-bold">Kab / Kota Kegiatan</label>
                    {!! Form::select('kabupaten_kota_id', [], null, ['class' => 'form-control', 'id' => 'kabupaten_kota_id', 'placeholder' => 'Kab / Kota Kegiatan']) !!}
                    <input type="hidden" id="kabupaten_kota_value" value="{{ !empty($data_pelatihan->kabupaten_kota_id) ? $data_pelatihan->kabupaten_kota_id : '' }}">
                    <div class="text-danger">
                        @if($errors->has('kabupaten_kota_id'))
                            {{ $errors->first('kabupaten_kota_id') }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label class="font-weight-bold">Tempat Pelaksanaan</label>
                    {!! Form::text('tempat', null, ['class' => 'form-control', 'placeholder' => 'Tempat Pelaksanaan']) !!}
                    <div class="text-danger">
                        @if($errors->has('tempat'))
                            {{ $errors->first('tempat') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-row">     
                <div class="col-md-6 mb-3">
                    <label class="font-weight-bold">Tanggal Mulai</label>
                    {!! Form::date('tanggal_mulai', !empty($data_pelatihan->tanggal_mulai) ? $data_pelatihan->tanggal_mulai->format('Y-m-d') : null, ['class' => 'form-control']) !!}
                    <div class="text-danger">
                        @if($errors->has('tanggal_mulai'))
                            {{ $errors->first('tanggal_mulai') }}
                        @endif
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label class="font-weight-bold">Tanggal Selesai</label>
                    {!! Form::date('tanggal_selesai', !empty($data_pelatihan->tanggal_selesai) ? $data_pelatihan->tanggal_selesai->format('Y-m-d') : null, ['class' => 'form-control']) !!}
                    <div class="text-danger">
                        @if($errors->has('tanggal_selesai'))
                            {{ $errors->first('tanggal_selesai') }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Dalam Rangka</label>
                {!! Form::select('dalam_rangka_id', $list_dalam_rangka, null, ['class' => 'form-control', 'placeholder' => 'Dalam Rangka']) !!}
                <div class="text-danger">
                    @if($errors->has('dalam_rangka_id'))
                        {{ $errors->first('dalam_rangka_id') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Dalam Rangka (Deskripsi)</label>
                {!! Form::textarea('dalam_rangka_des', null, ['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Dalam Rangka (Deskripsi)']) !!}
                <div class="text-danger">
                    @if($errors->has('dalam_rangka_des'))
                        {{ $errors->first('dalam_rangka_des') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Dukungan Sektor</label>
                {!! Form::select('dukungan_sektor_id', $list_dukungan_sektor, null, ['class' => 'form-control', 'placeholder' => 'Dukungan Sektor']) !!}
                <div class="text-danger">
                    @if($errors->has('dukungan_sektor_id'))
                        {{ $errors->first('dukungan_sektor_id') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Metode Pelaksanaan</label>
                {!! Form::select('metode_pelaksanaan_id', $list_metode_pelaksanaan, null, ['class' => 'form-control', 'id' => 'metode_pelaksanaan','placeholder' => 'Metode Pelaksanaan']) !!}
                <div class="text-danger">
                    @if($errors->has('metode_pelaksanaan_id'))
                        {{ $errors->first('metode_pelaksanaan_id') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3" id="swakelola">
        <div class="card-header">
            <strong>KHUSUS KEGIATAN SWAKELOLA</strong>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label class="font-weight-bold">Penanggung Jawab (Eselon III)</label>
                {!! Form::text('penanggung_jawab_swakelola', null, ['class' => 'form-control', 'placeholder' => 'Penanggung Jawab (Eselon III)']) !!}
                <div class="text-danger">
                    @if($errors->has('penanggung_jawab_swakelola'))
                        {{ $errors->first('penanggung_jawab_swakelola') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Pelaksana</label>
                {!! Form::text('pelaksana_swakelola', null, ['class' => 'form-control', 'placeholder' => 'Pelaksana']) !!}
                <div class="text-danger">
                    @if($errors->has('pelaksana_swakelola'))
                        {{ $errors->first('pelaksana_swakelola') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">No HP Pelaksana</label>
                {!! Form::text('no_hp_pelaksana_swakelola', null, ['class' => 'form-control', 'placeholder' => 'No HP Pelaksana']) !!}
                <div class="text-danger">
                    @if($errors->has('no_hp_pelaksana_swakelola'))
                        {{ $errors->first('no_hp_pelaksana_swakelola') }}
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3" id="kontraktual">
        <div class="card-header">
            <strong>KHUSUS KEGIATAN KONTRAKTUAL</strong>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label class="font-weight-bold">Nama Perusahaan</label>
                {!! Form::text('nama_perusahaan', null, ['class' => 'form-control', 'placeholder' => 'Nama Perusahaan']) !!}
                <div class="text-danger">
                    @if($errors->has('nama_perusahaan'))
                        {{ $errors->first('nama_perusahaan') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Direktur Utama</label>
                {!! Form::text('direktur_utama', null, ['class' => 'form-control', 'placeholder' => 'Direktur Utama']) !!}
                <div class="text-danger">
                    @if($errors->has('direktur_utama'))
                        {{ $errors->first('direktur_utama') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">No. HP</label>
                {!! Form::text('no_hp', null, ['class' => 'form-control', 'placeholder' => 'No. HP']) !!}
                <div class="text-danger">
                    @if($errors->has('no_hp'))
                        {{ $errors->first('no_hp') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Penanggung Jawab (Eselon III)</label>
                {!! Form::text('penanggung_jawab_kontraktual', null, ['class' => 'form-control', 'placeholder' => 'Penanggung Jawab (Eselon III)']) !!}
                <div class="text-danger">
                    @if($errors->has('penanggung_jawab_kontraktual'))
                        {{ $errors->first('penanggung_jawab_kontraktual') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">Pelaksana</label>
                {!! Form::text('pelaksana_kontraktual', null, ['class' => 'form-control', 'placeholder' => 'Pelaksana']) !!}
                <div class="text-danger">
                    @if($errors->has('pelaksana_kontraktual'))
                        {{ $errors->first('pelaksana_kontraktual') }}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold">No HP Pelaksana</label>
                {!! Form::text('no_hp_pelaksana_kontraktual', null, ['class' => 'form-control', 'placeholder' => 'No HP Pelaksana']) !!}
                <div class="text-danger">
                    @if($errors->has('no_hp_pelaksana_kontraktual'))
                        {{ $errors->first('no_hp_pelaksana_kontraktual') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="form-group mt-3">
        {!! Form::submit($button, ['class' => 'btn btn-primary btn-block']) !!}
    </div>