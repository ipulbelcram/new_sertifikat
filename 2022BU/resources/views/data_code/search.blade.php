<style>
    .active-link {
        background: #007bff !important;
        color: white !important;
    }
</style>

@extends('template')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">HISTORY PELATIHAN PESERTA</h1>
</div>

@if(!empty($nama_peserta))
<table class="mb-4">
    <tr>
        <th>NIK</th>
        <td class="px-4">:</td>
        <td>{{ $nama_peserta->nik }}</td>
    </tr>

    <tr>
        <th>Nama</th>
        <td class="px-4">:</td>
        <td>{{ $nama_peserta->nama }}</td>
    </tr>

    <tr>
        <th>Tempat, Tanggal lahir</th>
        <td class="px-4">:</td>
        <td>{{ $nama_peserta->tempat_lahir }}, {{ strftime("%d %B %Y", strtotime($nama_peserta->tanggal_lahir)) }}</td>
    </tr>

    <tr>
        <th>Alamat</th>
        <td class="px-4">:</td>
        <td>{{ $nama_peserta->alamat }}</td>
    </tr>

    <tr>
        <th>No Hp</th>
        <td class="px-4">:</td>
        <td>{{ $nama_peserta->no_telephone }}</td>
    </tr>
</table>
<table class="table table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th style="width:25%;">Nama Pelatihan</th>
            <th>Tanggal</th>
            <th>Tempat</th>
            <th>Nama</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 1; @endphp
        @foreach($list_data as $data)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data->nama_pelatihan }}</td>
                <td>{{ strftime("%d %B %Y", strtotime($data->tanggal_mulai)) }} s.d {{ strftime("%d %B %Y", strtotime($data->tanggal_selesai)) }}</td>
                <td>{{ $data->tempat }}</td>
                <td>{{ $data->nama }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@else 
    <p>Tidak Ada Data di Temukan</p>
@endif
@stop