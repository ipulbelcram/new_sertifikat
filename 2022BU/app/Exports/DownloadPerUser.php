<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class DownloadPerUser implements FromView, ShouldAutoSize
{
    public function __construct(string $user_id = null)
    {
        $this->user_id = $user_id;
    }
    
    public function view(): View
    {
        if(!empty($this->user_id)) {
            $data =  DB::table('vw_all_data_code')->where('user_id', $this->user_id)->get();
        } else {
            $data =  DB::table('vw_all_data_code')->get();
        }
        return view('excel.workingPaper', [
            'data' => $data
        ]);
    }
}