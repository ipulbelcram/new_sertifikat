@extends('dashboard-template')
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-8">
            <h4 class="font-weight-bold">PROFIL USAHA PESERTA DILATIH</h4>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <!-- STATUS USAHA PESERTA -->
        <div class="col-lg-6 col-md-12 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">A. Status Usaha Peserta</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Status Usaha</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_peserta_status_us = 0;
                                $persentase_status = 0;
                            ?>
                            @foreach($status_usaha as $status_usaha)
                            <?php 
                                $total_peserta_status_us += $status_usaha->total_peserta; 
                                $persentase_status += $status_usaha->persentase;
                            ?>
                            <tr>
                                <td>{{ $status_usaha->status_usaha }}</td>    
                                <td class="text-center">{{ number_format($status_usaha->total_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $status_usaha->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_peserta_status_us,0,',','.') }}</td>
                                <td>{{ round($persentase_status) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- SEKTOR USAHA PESERTA -->
        <div class="col-lg-6 col-md-12 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">B. Sektor Usaha Peserta</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Sektor Usaha Koperasi/ UMKM</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_peserta_sektor_us = 0; 
                                $persentase_sektor_us = 0;
                            ?>
                            @foreach($sektor_usaha_peserta as $sektor_usaha)
                            <?php 
                                $total_peserta_sektor_us += $sektor_usaha->total_peserta; 
                                $persentase_sektor_us += $sektor_usaha->persentase;
                            ?>
                            <tr>
                                <td>{{ $sektor_usaha->bidang_usaha }}</td>    
                                <td class="text-center">{{ number_format($sektor_usaha->total_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $sektor_usaha->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_peserta_sektor_us,0,',','.') }}</td>
                                <td>{{ round($persentase_sektor_us) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <!-- JENIS USAHA KOPERASI PESERTA -->
        <div class="col-lg-6 col-md-12 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">C. Jenis Usaha Koperasi Peserta</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Jenis Usaha Koperasi</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_peserta_jenis_usaha_kop = 0; 
                                $persentase_jenis_usaha_kop = 0;
                            ?>
                            @foreach($jenis_usaha_koperasi as $jenis_usaha_koperasi)
                            <?php 
                                $total_peserta_jenis_usaha_kop += $jenis_usaha_koperasi->total_peserta; 
                                $persentase_jenis_usaha_kop += $jenis_usaha_koperasi->persentase;
                            ?>
                            <tr>
                                <td>{{ $jenis_usaha_koperasi->jenis_usaha_koperasi }}</td>    
                                <td class="text-center">{{ number_format($jenis_usaha_koperasi->total_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $jenis_usaha_koperasi->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_peserta_jenis_usaha_kop,0,',','.') }}</td>
                                <td>{{ round($persentase_jenis_usaha_kop) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- JENIS USAHA UMKM PESERTA -->
        <div class="col-lg-6 col-md-12 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">D. Jenis Usaha UMKM Peserta</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Jenis Usaha UMKM</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_peserta_jenis_usaha_u = 0; 
                                $persentase_jenis_usaha_u = 0;
                            ?>
                            @foreach($jenis_usaha_umkm as $jenis_usaha_umkm)
                            <?php 
                                $total_peserta_jenis_usaha_u += $jenis_usaha_umkm->total_peserta; 
                                $persentase_jenis_usaha_u += $jenis_usaha_umkm->persentase;
                            ?>
                            <tr>
                                <td>{{ $jenis_usaha_umkm->jenis_umkm }}</td>    
                                <td class="text-center">{{ number_format($jenis_usaha_umkm->total_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $jenis_usaha_umkm->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_peserta_jenis_usaha_u,0,',','.') }}</td>
                                <td>{{ round($persentase_jenis_usaha_u) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <!-- LAMA USAHA -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">E. Lama Usaha UMKM Peserta</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Lama Usaha UMKM</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_peserta_lama_usaha = 0; 
                                $persentase_lama_usaha = 0;
                            ?>
                            @foreach($lama_usaha as $lama_usaha)
                            <?php 
                                $total_peserta_lama_usaha += $lama_usaha->total_peserta; 
                                $persentase_lama_usaha += $lama_usaha->persentase;
                            ?>
                            <tr>
                                <td>{{ $lama_usaha->lama_usaha }}</td>    
                                <td class="text-center">{{ number_format($lama_usaha->total_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $lama_usaha->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_peserta_lama_usaha,0,',','.') }}</td>
                                <td>{{ round($persentase_lama_usaha) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection