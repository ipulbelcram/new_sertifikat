<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Params extends Model
{
    protected $table = "params";
    protected $fillable = [
        'pid', 'category_params', 'params', 'active', 'order',
    ];

    public $timestamps = false;
}
