<style>
    .active-link {
        background: #007bff !important;
        color: white !important;
    }

    .sidebar .nav-link .feather {
        
    }
</style>
<nav class="col-md-2 d-none d-md-block bg-light sidebar mt-md-3">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <h6><a href="#" class="nav-link active"> Hello, {{ Auth::user()->name }} </a></h6>
                @php if(!empty($halaman) && $halaman == "Kosong") {
                    $active = "nav-link active-link";
                } else {
                    $active = "nav-link";
                }
                @endphp
                
                <a class="{{ $active }}" href="{{ url('dashboard') }}">                    
                    <i class="fa fa-home" aria-hidden="true"></i> Home <span class="sr-only">(current)</span>
                </a>
                @yield('list_menu')
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-3 text-muted">
                    <span>Menu Pencarian</span>
                </h6>
                <div class="col-md-12">
                    {!! Form::open(['url' => 'pelatihan/cari', 'method' => 'GET']) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('cari_pelatihan', old('cari_pelatihan'), ['class' => 'form-control', 'placeholder' => 'Cari Kegiatan']) !!}
                        <div class="input-group-append">
                            {!! Form::button('Cari', ['type' => 'submit', 'class' => 'btn btn-outline-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="input-group col-md-12">
                    {!! Form::open(['url' => 'pelatihan/cari_code', 'method' => 'GET']) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('cari_code', old('cari_code'), ['class' => 'form-control', 'placeholder' => 'Cari Nama']) !!}
                        <div class="input-group-append">
                            {!! Form::button('Cari', ['type' => 'submit', 'class' => 'btn btn-outline-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </li>
        </ul>
    </div>
</nav>