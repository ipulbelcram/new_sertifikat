@extends('template')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2 text-uppercase">DATA CODE {{ $data_pelatihan->nama_pelatihan }}</h1>
</div>
{!! Form::open(['url' => 'pelatihan/code']) !!}
<div class="form-row">
    <div class="col-md-11">
        {!! Form::hidden('pelatihan_id', $pelatihan_id) !!}
        {!! Form::text('code_pelatihan', null, ['class' => 'form-control', 'placeholder' => 'Code Pelatihan...']) !!}
        @if($errors->has('code_pelatihan'))
            {{ $errors->first('code_pelatihan') }}
        @endif
    </div>
    <div class="col">
        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
{!! Form::close() !!}
<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Code Pelatihan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data_code_pelatihan as $code_pelatihan)
            <tr>
                <td></td>
                <td>{{ $code_pelatihan->code_pelatihan }}</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm" id="qrButton" data-code ="{{ url('/pelatihan/code_detail/'.$code_pelatihan->code_pelatihan) }}">QrCode</a>
                    <a href="{{ url('pelatihan/create_data/'.$code_pelatihan->id) }}" class="btn btn-primary btn-sm">Create Data</a>
                    <a href="{{ url('pelatihan/detail_data_code/'.$code_pelatihan->code_pelatihan) }}" class="btn btn-primary btn-sm" target="_blank">Detail</a>
                    <a href="{{ url('pelatihan/edit_data_code/'.$code_pelatihan->code_pelatihan) }}" class="btn btn-primary btn-sm">Edit Data</a>
                    <a href="{{ url('/pelatihan/print_data_code/'.$code_pelatihan->code_pelatihan) }}" class="btn btn-primary btn-sm" target="_blank">Print</a>
                    {!! Form::open(['method'=>'DELETE', 'action' => ['CodePelatihanController@destroy',$code_pelatihan->id, $code_pelatihan->code_pelatihan]]) !!}
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure you want to delete this item?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="ModalCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@stop

