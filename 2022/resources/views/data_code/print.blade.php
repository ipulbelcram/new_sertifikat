<link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<div class="col-lg-12" style="margin-top:70px; margin-left:70px;">
    @foreach($code as $codePelatihan)
    <div>
        @php
            $link = "http://$_SERVER[HTTP_HOST]/2021/pelatihan/detail_data_code/".$codePelatihan->code_pelatihan;
        @endphp
        <div style="margin-left:25px; transform: rotate(90deg); transform-origin: left top 0">
            <h3>KODE : {{ $codePelatihan->code_pelatihan }}</h3>
        </div>
        <div style="margin-top:-70px;">
            {!! QrCode::size(230)->generate($link) !!}
        </div>
    </div>
    @endforeach
        <p>Diberikan Kepada : {{ $code_pelatihan->nama }}</p>
        <p>Telah Mengikuti : {{ $code_pelatihan->pelatihan_yang_diikuti }}</p>
        <p>Pada Tanggal : {{  $code_pelatihan->dari_tanggal }} - {{ $code_pelatihan->sampai_tanggal }}</p>
        <p>Di : {{ $code_pelatihan->lokasi_pelatihan }}</p>
</div>
