<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PelatihanResource;
use App\Http\Requests\PelatihanRequest;
use App\Pelatihan;

class PelatihanApi extends Controller
{
    public function store(Request $request)
    {
        if(!empty($request->ref_asn_pelatihan_id)) {
            $jumlah_pelatihan = Pelatihan::where('ref_asn_pelatihan_id', $request->ref_asn_pelatihan_id)->count();
            if($jumlah_pelatihan < 1) {
                $input['user_id'] = $request->user_id;
                $input['jenis_pelatihan_id'] = $request->jenis_pelatihan_id;
                $input['nama_pelatihan'] = $request->nama_pelatihan;
                $input['tempat'] = $request->tempat;
                $input['tanggal_mulai'] = $request->tanggal_mulai;
                $input['tanggal_selesai'] = $request->tanggal_selesai;
                $input['provinsi_id'] = $request->provinsi_id;
                $input['kabupaten_kota_id'] = $request->kabupaten_kota_id;
                $input['dalam_rangka_id'] = $request->dalam_rangka_id;
                $input['dalam_rangka_des'] = $request->dalam_rangka_des;
                $input['dukungan_sektor_id'] = $request->dukungan_sektor_id;
                $input['metode_pelaksanaan_id'] = $request->metode_pelaksanaan_id;
                $input['penanggung_jawab_swakelola'] = $request->penanggung_jawab_swakelola;
                $input['pelaksana_swakelola'] = $request->pelaksana_swakelola;
                $input['no_hp_pelaksana_swakelola'] = $request->no_hp_pelaksana_swakelola;
                $input['nama_perusahaan'] = $request->nama_perusahaan;
                $input['direktur_utama'] = $request->direktur_utama;
                $input['no_hp'] = $request->no_hp;
                $input['penanggung_jawab_kontraktual'] = $request->penanggung_jawab_kontraktual;
                $input['pelaksana_kontraktual'] = $request->pelaksana_kontraktual;
                $input['no_hp_pelaksana_kontraktual'] = $request->no_hp_pelaksana_kontraktual;
                $input['status'] = 'N';
                $input['ref_asn_pelatihan_id'] = $request->ref_asn_pelatihan_id;
        
                $pelatihan = Pelatihan::create($input);
                return new PelatihanResource($pelatihan);
            } else {
                return response()->json(['alert' => 'pelatihan sudah ada']);
            }
        }
        
    }
}
