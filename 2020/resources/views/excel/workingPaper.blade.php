<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
            <tr>
                <th>Jenis Kegiatan</th>
                <th>Nama Kegiatan</th>
                <th>Provinsi</th>
                <th>Kab/Kota Pelatihan</th>
                <th>Tempat Pelaksanaan</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th>Dalam Rangka</th>
                <th>Dalam Rangka (Deskripsi)</th>
                <th>Dukungan Sektor</th>
                <th>Metode Pelaksanaan</th>
    
                <th>Nama Perusahaan</th>
                <th>Direktur Utama</th>
                <th>No. HP</th>
                <th>Penanggung Jawab (Eselon III)</th>
                <th>Pelaksana</th>
                <th>No HP Pelaksana</th>
    
                <th>No</th>
                <th>Nama Lengkap</th>
                <th>Nomor KTP</th>
                <th>Status</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Agama</th>
                <th>Pendidikan Terakhir</th>
                <th>Nomor NPWP</th>
                <th>Alamat Rumah</th>
                <th>Email</th>
                <th>Telp/Hp</th>
                <th>Kab/Kota Peserta</th>
                <th>Pekerjaan/Jabatan</th>
                <th>Status Usaha</th>
                <th>Sektor Usaha Koperasi/ UMKM</th>
                <th>Nama Koperasi/UMKM</th>
                <th>Alamat Koperasi/UMKM</th>
                <th>Nomor Induk Koperasi(NIK)</th>
                <th>Jenis Usaha Koperasi/ UMKM</th>
                <th>Bidang Usaha UMKM</th>
                <th>Lama Usaha UMKM</th>
                <th>Jumlah Tenaga Kerja Koperasi/ UMKM</th>
                <th>Omset Koperasi per Tahun/ Omset Usaha per Bulan</th>
                <th>Qr Code No</th>
                <th>Qr Code Date</th>
                <th>Entry Date</th>
                <th>User</th>
                <th>Status Foto</th>
            </tr>
            @php 
                $no=1 
            @endphp
            @foreach($data as $item)
            @php 
                if(!empty($item->foto)) {
                    $status_foto = "Ada";
                } else {
                    $status_foto = "Tidak Ada";
                }
    
                if(!empty($item->nik_koperasi)) {
                    if($item->nik_koperasi == 'true') {
                        $nik_koperasi = 'Ada';
                    } else {
                        $nik_koperasi = 'Tidak Ada';
                    }
                } else {
                    $nik_koperasi = '';
                }
            @endphp
            <tr>
                <td>{{ $item->jenis_pelatihan }}</td>
                <td>{{ $item->nama_pelatihan }}</td>
                <td>{{ $item->provinsi }}</td>
                <td>{{ $item->kabupaten_kota }}</td>
                <td>{{ $item->tempat }}</td>
                <td>{{ date('d-m-Y', strtotime($item->tanggal_mulai)) }}</td>
                <td>{{ date('d-m-Y', strtotime($item->tanggal_selesai)) }}</td>
    
                <td>{{ $item->dalam_rangka }}</td>
                <td>{{ $item->dalam_rangka_des }}</td>
                <td>{{ $item->dukungan_sektor }}</td>
                <td>{{ $item->metode_pelaksanaan }}</td>
                <td>{{ $item->nama_perusahaan }}</td>
                <td>{{ $item->direktur_utama }}</td>
                <td>'{{ $item->no_hp }}</td>
                <td>{{ $item->penanggung_jawab_swakelola }} {{ $item->penanggung_jawab_kontraktual }}</td>
                <td>{{ $item->pelaksana_swakelola }} {{ $item->pelaksana_kontraktual }}</td>
                <td>'{{ $item->no_hp_pelaksana_swakelola }} {{ $item->no_hp_pelaksana_kontraktual }}</td>
    
                <td>{{ $no++ }}</td>
                <td>{{ $item->nama }}</td>
                <td>'{{ $item->nik }}</td>
                <td>{{ $item->status }}</td>
                <td>{{ $item->jenis_kelamin }}</td>
                <td>{{ $item->tempat_lahir }}</td>
                <td>{{ date('d-m-Y', strtotime($item->tanggal_lahir)) }}</td>
                <td>{{ $item->agama }}</td>
                <td>{{ $item->pendidikan }}</td>
                <td>'{{ $item->npwp }}</td>
                <td>{{ $item->alamat }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->no_telephone }}</td>
                <td>{{ $item->kabupaten_kota_peserta }}</td>
                <td>{{ $item->pekerjaan_jabatan }}</td>
                <td>{{ $item->status_usaha }}</td>
                <td>{{ $item->bidang_usaha_koperasi }} {{ $item->sektor_usaha }}</td>
                <td>{{ $item->nama_koperasi }} {{ $item->nama_umkm }}</td>
                <td>{{ $item->alamat_koperasi }} {{ $item->alamat_umkm }}</td>
                <td>{{ $nik_koperasi }}</td>
                <td>{{ $item->bidang_koperasi }} {{ $item->jenis_usaha }}</td>
                <td>{{ substr($item->bidang_usaha,5) }}</td>
                <td>{{ $item->lama_usaha }}</td>
                <td>'{{ $item->tenaga_kerja_koperasi }}{{ $item->tenaga_kerja_umkm }}</td>
                <td>'{{ $item->omset_koperasi }} {{ $item->omset_usaha_perbulan }}</td>
                <td>{{ $item->qr_code_no }}</td>
                <td>{{ !empty($item->tanggal_input) ? date('d-m-Y', strtotime($item->tanggal_input)) : '' }}</td>
                <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                <td>{{ $item->user_type }}</td>
                <td>{{ $status_foto }}</td>
            </tr>
            @endforeach
        </table>
    </div>

    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="{{ asset('js/exportCsv.js') }}"></script>
    <script>
        $(document).ready(function() {
            exportTableToExcel('data', 'Format Working Paper')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script>
</body>
</html>