@extends('template')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    @foreach($data_pelatihan as $pelatihan)
        @php
            $nama_pelatihan = $pelatihan->nama_pelatihan;
            $tempat_pelatihan = $pelatihan->tempat;
            $tanggal_mulai = $pelatihan->tanggal_mulai;
            $tanggal_selesai = $pelatihan->tanggal_selesai;
            $kabupaten_kota_pelatihan = $pelatihan->kabupaten_kota->kab_kota;
            $provinsi_pelatihan = $pelatihan->provinsi->provinsi;
            $pelatihan_id = $pelatihan->id;
            $status_pelatihan = $pelatihan->status;
            $no = ($data_code->perPage() * $data_code->currentPage()) - ($data_code->perPage() - 1);
        @endphp
    @endforeach
        <h1 class="h2">LIST PESERTA {{ $nama_pelatihan }}</h1>
    </div>
    <div>
        <table>
            <tr>
                <td><strong>Tempat/ Hotel Kegiatan</strong></td>
                <td class="px-4">:</td>
                <td>{{ $tempat_pelatihan }}</td>
            </tr>
            <tr>
                <td><strong>Tanggal Pelaksanaan</strong></td>
                <td class="px-4">:</td>
                <td>{{ strftime("%d %B %Y", strtotime($tanggal_mulai)) }} s.d {{ strftime("%d %B %Y", strtotime($tanggal_selesai)) }}</td>
            </tr>

            <tr>
                <td><strong>Kab/ Kota Kegiatan</strong></td>
                <td class="px-4">:</td>
                <td>{{ $kabupaten_kota_pelatihan }}</td>
            </tr>

            <tr>
                <td><strong>Provinsi</strong></td>
                <td class="px-4">:</td>
                <td>{{ $provinsi_pelatihan }}</td>
            </tr>
        </table>
    </div>
    <br>
    <div class="float-left">
        <a href="{{ url('excel/'.$pelatihan_id.'/format_laporan') }}" class="btn btn-primary btn-sm">Format Laporan</a>
    </div>
    
    <div class="float-left">
        <a href="{{ url('pelatihan/validasi_foto/'.$pelatihan_id) }}" target="_blank" class="btn btn-success btn-sm ml-3">Validasi Foto</a>
    </div>
    @if($status_pelatihan != "Y" OR Auth::user()->code_user == "Z")
        <div class="float-right">
            <a href="{{ url('pelatihan/create_data/'.$pelatihan_id) }}" class="btn btn-success btn-sm mb-3">Tambah Peserta</a>
        </div>
    @endif
    @if($status_pelatihan != "Y")
        <div style="padding-right:125px">
        @php
        foreach($ket_pelatihan as $pelatihan_ket){
            $status_Y = $pelatihan_ket->status_Y;
            $jumlah_peserta = $pelatihan_ket->jumlah_peserta;
        }
        @endphp
            @if(Auth::user()->code_user == "Z" && $status_Y >= $jumlah_peserta && $jumlah_peserta != 0)
                <a href="{{ url('pelatihan/selesai/'.$pelatihan_id) }}" class="float-right btn btn-info btn-sm">Lock</a>
            @else
                @if(Auth::user()->code_user == "Z")
                    <button type="button" class="float-right btn btn-danger btn-sm" disabled>Data Belum Lengkap</button>
                @endif
            @endif
        </div>
    @else
        @if(Auth::user()->code_user == "Z")        
            <div style="padding-right:125px">
                <a href="{{ url('pelatihan/unlock/'.$pelatihan_id) }}" class="float-right btn btn-info btn-sm">Unlock</a>
            </div>
        @else 
            <div>
                <button type="button" class="float-right btn btn-info btn-sm" disabled>Telah Terselesaikan</button>
            </div>
        @endif
    @endif
    <br><br>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th style="width:15%">Nama</th>
                <th style="width:13%">Jenis Kelamin</th>
                <th>Usia</th>
                <th style="width:13%">Telp/ HP</th>
                <th style="width:15%">Code</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <th>Jumlah</th>
            <th>{{ $jumlah_data_code }} Peserta</th>
        </tfoot>
        <tbody>
            @foreach($data_code as $data)
                <tr>   
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nama }}</td>
                    <td class="text-center">{{ $data->jenis_kelamin }}</td>
                    <td>{{ $data->usia }} Th</td>
                    @php
                        if(!empty($data->no_telephone)){
                            $no_telp = $data->no_telephone;
                        } else {
                            $no_telp = "-";
                        }
                    @endphp
                    <td>{{ $no_telp }}</td>
                    @php
                        if(!empty($data->code_pelatihan->no_urut) && $data->code_pelatihan->status == 'Y') {
                            $tanggal_input = $data->code_pelatihan->tanggal_input;
                            list($tahun, $bulan, $tanggal) = explode('-', $tanggal_input);
                            $QrCode = $data->code_pelatihan->code_user.''.$tahun.''.$bulan.''.$tanggal.''.sprintf("%04d", $data->code_pelatihan->no_urut);
                        } else {
                            $QrCode = "(belum membuat qrcode)";
                        }
                    @endphp
                    <td>{{ $QrCode }}</td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu">
                                @if($status_pelatihan != "Y" OR Auth::user()->code_user == "Z")
                                    <a href="{{ url('pelatihan/edit_data_code/'.$data->id.'/'.$data_code->currentPage()) }}" class="dropdown-item">Edit</a>
                                @endif
                                @if(Auth::user()->code_user == 'Z')
                                    <a href="{{ url('pelatihan/updateQrcode/'.$data->id.'/'.$data_code->currentPage()) }}" class="dropdown-item">Buat QrCode</a>
                                @endif
                                @if(!empty($data->code_pelatihan->data_code_id))
                                    <a href="{{ url('pelatihan/detail_data_code/'.$QrCode) }}" class="dropdown-item" target="_blank">Lihat QrCode</a>
                                    <a href="{{ url('pelatihan/print_code/'.$data->id.'/Y') }}" class="dropdown-item" target="_blank">Print Dengan Tanggal</a>
                                    <a href="{{ url('pelatihan/print_code/'.$data->id.'/N') }}" class="dropdown-item" target="_blank">Print Tanpa Tanggal</a>
                                    <a href="{{ url('pelatihan/print_code/'.$data->id.'/F') }}" class="dropdown-item" target="_blank">Print Dengan Foto</a>
                                    <a href="{{ route('data_pelatihan.download_pdf', ['data_code_id' => $data->id, 'status' => 'Y']) }}" class="dropdown-item" target="_blank">Sertifikat PDF (Background)</a>
                                    <a href="{{ route('data_pelatihan.download_pdf', ['data_code_id' => $data->id, 'status' => 'N']) }}" class="dropdown-item" target="_blank">Sertifikat PDF (No Background)</a>
                                @endif

                                @if($status_pelatihan != "Y" OR Auth::user()->code_user == "Z")
                                    <a href="{{ url('pelatihan/delete_data_code/'.$pelatihan_id.'/'.$data->id.'/'.$data_code->currentPage()) }}" class="dropdown-item" onClick="return confirm('Apa Kamu Yakin Ingin Menghapusnya ?')">Delete</a>
                                @endif
                            </div>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Share
                            </button>
                            <div class="dropdown-menu">
                                @php
                                    $rm_no = substr($data->no_telephone, 1);
                                    $wa_no = substr_replace($rm_no, "62", 0, 0);
                                    $wa_sent = "https://api.whatsapp.com/send?phone=". $wa_no ."&text=Silahkan%20download%20sertifikat%20anda%20melalui%20link%20dibawah%20ini%20:%20%0A%0A";
                                    $wa_sent_bg = $wa_sent . route('data_pelatihan.download_pdf', ['data_code_id' => $data->id, 'status' => 'Y']);
                                    $wa_sent_nobg = $wa_sent . route('data_pelatihan.download_pdf', ['data_code_id' => $data->id, 'status' => 'N']);
                                @endphp
                                <a href="{{ $wa_sent_bg }}" class="dropdown-item" target="_blank">Share Whatsapp Sertifikat Background</a>
                                <a href="{{ $wa_sent_nobg }}" class="dropdown-item" target="_blank">Share Whatsapp Sertifikat No Background</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $data_code->links() }}
    </div>
@stop