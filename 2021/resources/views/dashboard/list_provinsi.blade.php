@extends('dashboard-template')
@section('content')
<!-- Page Header -->
<div class="page-header row no-gutters py-4">
  <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
    <h3 class="page-title">List Provinsi</h3>
  </div>
</div>
<!-- End Page Header -->

<!-- list By provinsi -->
<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
  <div class="card card-small h-100">
    <div class="card-header border-bottom">
      <h6 class="m-0">Peserta Berdasarkan Provinsi</h6>
    </div>
    <div class="card-body d-flex py-0">
      <canvas height="300" class="list-by-provinsi m-auto"></canvas>
    </div>
    
  </div>
</div>
<!-- End List By Provinsi -->

<!-- Data Provinsi -->
<div class="row">
  <div class="col-lg-12 mb-4">
    <div class="card card-small country-stats">
      <div class="card-header border-bottom">
        <h6 class="m-0">Data Per Provinsi</h6>
        <div class="block-handle"></div>
      </div>
      <div class="card-body p-0">
        <table class="table m-0">
          <tbody>
          @foreach($data_perprovinsi as $perprovinsi)
            <tr>
              <td><a href="{{ url('dashboard2/'.$perprovinsi->provinsi_id.'/pelatihan_perprovinsi') }}">{{ $perprovinsi->provinsi }}</a> {{ $perprovinsi->persentase_jumlah_data }}% </td>
              <td class="text-right">{{ number_format($perprovinsi->jumlah_pelatihan_perprovinsi,0,',','.') }} Pelatihan</td>
              <td class="text-right">{{ number_format($perprovinsi->jumlah_data_perprovinsi,0,',','.') }} Peserta</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop