<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('qrcode/index', 'QrcodeController@index');

Route::get('/', function() {
    return redirect('/login');
});

Route::group(['middleware' => ['web']], function() {
    Route::get('dashboard', 'PelatihanController@index')->name('dashboard');
    Route::get('dashboard/{user_id}', 'PelatihanController@list_pelatihan');
    Route::get('pelatihan/tambah_pelatihan', 'PelatihanController@tambah');
    Route::post('pelatihan', 'PelatihanController@store');
    Route::get('pelatihan/edit_pelatihan/{id}', 'PelatihanController@edit');
    Route::patch('pelatihan/update_pelatihan/{id}', 'PelatihanController@update');
    Route::get('pelatihan/delete_pelatihan/{pelatihan_id}', 'PelatihanController@destroy');
    Route::get('pelatihan/cari', 'PelatihanController@search');
    Route::get('pelatihan/selesai/{pelatihan_id}', 'PelatihanController@update_status_pelatihan');
    Route::get('pelatihan/unlock/{pelatihan_id}', 'PelatihanController@unlock_status_pelatihan');
});

Route::group(['middleware' => ['web']], function() {
    Route::get('pelatihan/code_detail/{id}', 'CodePelatihanController@codeDetail');
    Route::get('pelatihan/updateQrcode/{data_code_id}/{currentpage}', 'CodePelatihanController@updateQrCode');
    Route::get('pelatihan/qrcode_and_lock/{id}/{currentpage}', 'PelatihanController@qrcodeAndLock');
    Route::get('pelatihan/print_code/{data_code_id}/{status_tanggal}', 'CodePelatihanController@print');
    Route::get('pelatihan/cari_code', 'CodePelatihanController@search');
});

Route::group(['middleware' => ['web']], function() {
    Route::get('pelatihan/list_data/{pelatihan_id}', 'DataPelatihanController@index')->name('data_peserta_pelatihan');
    Route::get('pelatihan/create_data/{pelatihan_id}', 'DataPelatihanController@create');
    Route::post('pelatihan/create_data', 'DataPelatihanController@store');
    Route::get('pelatihan/detail_data_code/{QrCode}', 'DataPelatihanController@detail')->name('detail_data_code');
    Route::get('pelatihan/edit_data_code/{data_code_id}/{currentpage}', 'DataPelatihanController@edit');
    Route::patch('pelatihan/edit_data_code/{data_code_id}/{currentpage}', 'DataPelatihanController@update');    
    Route::get('pelatihan/delete_data_code/{pelatihan_id}/{data_code_id}/{currentpage}', 'DataPelatihanController@destroy');
    Route::get('pelatihan/print_data_code/{data_code_id}', 'DataPelatihanController@print');
    Route::get('pelatihan/validasi_foto/{pelatihan_id}', 'DataPelatihanController@validasi_foto');
    Route::post('pelatihan/image_upload', 'DataPelatihanController@image_upload');
    Route::get('data_pelatihan/search_nik', 'DataPelatihanController@search_nik');
    Route::get('data_pelatihan/{data_code_id}/{status?}/download_sertifikat', 'DataPelatihanController@download_pdf')->name('data_pelatihan.download_pdf');
});


Route::group(['middleware' => ['web']], function() {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['middleware' => ['web']], function() {
    Route::get('dashboard2', 'DashboardController@index');
    Route::get('profile_kegiatan_pelatihan', 'DashboardController@profile_kegiatan_pelatihan')->name('profile_kegiatan_pelatihan');
    Route::get('profile_peserta_pelatihan', 'DashboardController@profile_peserta_pelatihan')->name('profile_peserta_pelatihan');
    Route::get('profile_usaha_peserta', 'DashboardController@profile_usaha_peserta')->name('profile_usaha_peserta');
    Route::get('penyebaran_peserta_pelatihan', 'DashboardController@penyebaran_peserta_pelatihan')->name('penyebaran_peserta_pelatihan');

    Route::get('dashboard2/user/{user_id}', 'DashboardController@dataByUser')->name('dashboard2.dataByUser');
    Route::get('dashboard2/{pelatihan_id}/list_data_perpelatihan', 'DashboardController@list_data_perprovinsi');
    Route::get('dashboard2/{provinsi_id}/pelatihan_perprovinsi', 'DashboardController@pelatihan_perprovinsi')->name('dashboard2.pelatihanPerProvinsi');
    Route::get('dashboard2/{user_id}/{pelatihan_id}/pelatihan_perprovinsi', 'DashboardController@pelatihan_provinsi_peruser');
    Route::get('dashboard2/list_provinsi', 'DashboardController@list_provinsi');
    Route::get('dashboard2/detail_peserta/{data_code_id}', 'DashboardController@detail_peserta');
});

Route::get('excel/{pelatihan_id}/format_laporan','ExcelController@export');
Route::get('excel/download_peruser/{user_id?}', 'ExcelController@download_peruser');