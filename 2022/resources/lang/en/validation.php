<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'nama_pelatihan' => [
            'required' => 'Kolom NISN harus di isi',
            'string' => 'Kolom NISN harus berupa string',
        ],

        'tempat' => [
            'required' => 'Kolom TEMPAT harus di isi',
            'string' => 'Kolom TEMPAT harus berupa string',
        ],

        'tanggal_mulai' => [
            'required' => 'Kolom TANGAL MULAI harus di isi',
            'date' => 'Kolom TANGGAL MUALAI harus harus di isi format tanggal yang benar',
        ],

        'tanggal_selesai' => [
            'required' => 'Kolom TANGAL SELESAI harus di isi',
            'date' => 'Kolom TANGGAL SELESAI harus harus di isi format tanggal yang benar',
        ],

        'pelatihan_id' => [
            'required' => 'Kolom PELATIHAN ID harus di isi',
            'string' => 'Kolom PELATIHAN ID harus berupa string',
        ],

        'nik' => [
            'required' => 'Kolom NIK harus di isi',
            'string' => 'Kolom NIK harus berupa string',
            'size' => 'Kolom NIK harus 16 Karakter'
        ],

        'nama' => [
            'required' => 'Kolom NAMA harus di isi',
            'string' => 'Kolom NAMA harus berupa string',
        ],

        'tempat_lahir' => [
            'string' => 'Kolom TEMPAT LAHIR harus berupa string',
        ],

        'tanggal_lahir' => [
            'date' => 'Kolom TANGGAL LAHIR harus format tanggal yg benar',
        ],                

        'jenis_kelamin' => [
            'required' => 'Kolom JENIS KELAMIN harus di isi',
            'in' => 'Kolom JENIS KELAMIN harus di isi L atau P',
        ],

        'status' => [
            'required' => 'Kolom STATUS harus di isi',
            'in' => 'Kolom STATUS harus di isi L atau M',
        ],

        'badan_usaha' => [
            'in' => 'Kolom BADAN USAHA harus di isi K atau U',
        ],

        'email' => [
            'string' => 'Kolom EMAIL harus berupa string',
        ],

        'no_telephone' => [
            'required' => 'Kolom NOMOR TELP/HP harus di isi',
            'numeric' => 'Kolom NO TELEPHONE harus berupa angka',
            'digits_between' => 'Kolom NO TELEPHONE min 10 sampai 15 karacter'
        ],

        'alamat' => [
            'string' => 'Kolom ALAMAT harus berupa string',
        ],

        'asal_lembaga' => [
            'required' => 'Kolom ASAL LEMBAGA harus di isi',
            'string' => 'Kolom ASAL LEMBAGA harus berupa string',
        ],

        'kabupaten_kota' => [
            'string' => 'Kolom KABUPATEN/KOTA harus berupa string',
        ],

        'rencana_usaha' => [
            'string' => 'Kolom RENCANA USAHA harus berupa string',
        ],

        'nama_koperasi' => [
            'string' => 'Kolom NAMA KOPERASI harus berupa string',
        ],

        'nik_koperasi' => [
            'string' => 'Kolom NIK Koperasi harus berupa string',
        ],

        'npwp' => [
            'string' => 'Kolom NOMOR NPWP harus berupa string',
        ],

        'lama_usaha' => [
            'string' => 'Kolom LAMA USAHA harus berupa string',
        ],

        'omset_usaha_perbulan' => [
            'string' => 'Kolom OMSET USAHA PERBULAN harus berupa string',
        ],

        'foto' => [
            'required' => 'Kolom FOTO Harus di isi',
            'image' => 'Kolom FOTO hanya boleh berisi file gamber',
            'max' => 'Kolom FOTO tidak boleh lebih dari 500 KB',
            'mimes' => 'Kolom FOTO hanya boleh di isi file *.jpg, *.jpeg, *.png',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
