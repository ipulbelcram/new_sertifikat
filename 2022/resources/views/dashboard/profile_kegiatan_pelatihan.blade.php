@extends('dashboard-template')
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-8">
            <h4 class="font-weight-bold">PROFIL KEGIATAN PELATIHAN</h4>
        </div>
    </div>
    <!-- End Page Header -->
    
    <div class="row">
        <!-- TUJUAN PELAKSANAAN TABLE -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">A. Tujuan Pelaksanaan</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Tujuan</th>
                            <th>Jumlah Pelatihan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_pelatihan_tujuan_pel = 0;
                                $total_peserta_tujuan_pel = 0;
                                $persentase_tujuan_pel = 0;
                            ?>
                            @foreach($tujuan_pelaksanaan as $tujuan_pelaksanaan)
                            <?php
                                $total_pelatihan_tujuan_pel += $tujuan_pelaksanaan->total_pelatihan;
                                $total_peserta_tujuan_pel += $tujuan_pelaksanaan->total_peserta;
                                $persentase_tujuan_pel += $tujuan_pelaksanaan->persentase;
                            ?>
                            <tr>
                                <td>{{ $tujuan_pelaksanaan->dalam_rangka }}</td>
                                <td class="text-center">{{ $tujuan_pelaksanaan->total_pelatihan }}</td>
                                <td class="text-center">{{ $tujuan_pelaksanaan->total_peserta }}</td>
                                <td class="text-center">{{ $tujuan_pelaksanaan->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_pelatihan_tujuan_pel,0,',','.') }}</td>
                                <td>{{ number_format($total_peserta_tujuan_pel,0,',','.') }}</td>
                                <td>{{ round($persentase_tujuan_pel) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- DUKUNGAN SEKTOR PRIORITAS -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">B. Dukungan Sektor Prioritas</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Sektor</th>
                            <th>Jumlah Pelatihan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php 
                                $total_pelatihan_dukungan_sektor = 0;
                                $total_peserta_dukungan_sektor = 0;
                                $persentase_dukungan_sektor = 0;
                            ?>
                            @foreach($dukungan_sektor as $sektor)
                            <?php
                                $total_pelatihan_dukungan_sektor += $sektor->total_pelatihan;
                                $total_peserta_dukungan_sektor += $sektor->total_peserta;
                                $persentase_dukungan_sektor += $sektor->persentase;
                            ?>
                            <tr>
                                <td>{{ $sektor->dukungan_sektor }}</td>
                                <td class="text-center">{{ $sektor->total_pelatihan }}</td>
                                <td class="text-center">{{ $sektor->total_peserta }}</td>
                                <td class="text-center">{{ $sektor->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="text-center font-weight-bold">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_pelatihan_dukungan_sektor,0,',','.') }}</td>
                                <td>{{ number_format($total_peserta_dukungan_sektor,0,',','.') }}</td>
                                <td>{{ round($persentase_dukungan_sektor) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <!-- METODE PELAKSANAAN PELATIHAN -->
        <div class="col-lg-6 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">C. Metode Pelaksanaan Pelatihan</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Metode Pelaksanan</th>
                            <th>Jumlah Pelatihan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_pelatihan_metode_pel = 0;
                                $total_peserta_metode_pel = 0;
                                $persentase_metode_pel = 0;
                            ?>
                            @foreach($metode_pelaksanaan as $metode_pelaksanaan)
                            <?php
                                $total_pelatihan_metode_pel += $metode_pelaksanaan->total_pelatihan;
                                $total_peserta_metode_pel += $metode_pelaksanaan->total_peserta;
                                $persentase_metode_pel += $metode_pelaksanaan->persentase;
                            ?>
                            <tr>
                                <td>{{ $metode_pelaksanaan->metode_pelaksanaan }}</td>
                                <td class="text-center">{{ $metode_pelaksanaan->total_pelatihan }}</td>
                                <td class="text-center">{{ !empty($metode_pelaksanaan->total_peserta) ? $metode_pelaksanaan->total_peserta : '0' }}</td>
                                <td class="text-center">{{ !empty($metode_pelaksanaan->persentase) ? $metode_pelaksanaan->persentase : '0' }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_pelatihan_metode_pel,0,',','.') }}</td>
                                <td>{{ number_format($total_peserta_metode_pel,0,',','.') }}</td>
                                <td>{{ round($persentase_metode_pel) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection