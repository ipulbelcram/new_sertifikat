<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ttd extends Model
{
    protected $table = 'ttds';
    protected $fillable = [
        'code_user',
        'unit',
        'name',
        'ttd_path'
    ];

    public $timestamps = false;
}
