<style>
    .active-link {
        background: #007bff !important;
        color: white !important;
    }
</style>
@extends('template')
@section('list_menu')
    @if(Auth::user()->code_user == 'Z')
        @foreach($list_users as $users)
            @php 
                if(!empty($halaman) && $halaman == $users->id){
                    $active = "nav-link active-link";
                } else {
                    $active = "nav-link";
                }
                
            @endphp
            <a href="{{ url('dashboard/'.$users->id) }}" class="{{ $active }}"> {{ $users->name }} </a>
        @endforeach
    @endif
@stop
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">DATA PELATIHAN 2022</h1>
    <h6>Total Peserta Rilis QR Code {{ !empty($total_data_terbarcode) ? number_format($total_data_terbarcode,'0',',','.') : "0" }} dari {{ !empty($jumlah_data_code) ? number_format($jumlah_data_code,0,',','.') : "0" }}</h6>
</div>
<div>
    <div>
        <h5 class="m-0">Kementerian Koperasi dan UKM Republik Indonesia</h5>
        <p style="font-size:15px;">{{ Auth::user()->name }}</p>
    </div>
</div>
<div>
    <div class="form-row mt-3">
        @if($id_user >= 1)
            <div class="form-group col-md-2">
                <a href="{{ url('excel/download_peruser/'.$id_user) }}" target="_blank" class="float-left btn btn-primary btn-sm">Format Working Paper</a>
            </div>
        @elseif(empty($id_user) && Auth::user()->id == 7) 
            <div class="form-group col-md-3">
                <a href="{{ url('excel/download_peruser') }}" target="_blank" class="float-left btn btn-primary btn-sm btn-block">Format Working Paper All</a>
            </div>
        @elseif(Auth::user()->code_user != 'Z')
            <div class="form-group col-md-2">
                <a href="{{ url('excel/download_peruser/'.Auth::user()->id) }}" target="_blank" class="float-left btn btn-primary btn-sm">Format Working Paper</a>
            </div>
        @endif
        @php 
            $url_sekarang = url()->current()
        @endphp
        <div class="form-group col-md-5">
        {!! Form::open(['url' => 'data_pelatihan/search_nik', 'method' => 'GET']) !!}
            <div class="input-group">
                {!! Form::number('kata_kunci', !empty($kata_kunci) ? $kata_kunci : '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Pencarian Nomor KTP']) !!}
                <div class="input-group-append">
                    {!! Form::button('Cari', ['type' => 'submit', 'class' => 'btn btn-sm btn-outline-primary']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        </div>

        <div class="form-group col-md-3">
            <select class="form-control-sm col-lg-12" name="filterby" onchange="location = this.value;">
                <option value="" hidden>Filter By</option>
                <option value="{{ $url_sekarang.'?filterby=sudah_semua' }}">Sudah Semua</option>
                <option value="{{ $url_sekarang.'?filterby=peserta_kosong' }}">Peserta Kosong</option>
                <option value="{{ $url_sekarang.'?filterby=belum_semua' }}">Belum Semua</option>
            </select>
        </div>
        @if(Auth::user()->user_level_id != 10)
        <div class="form-group col-md-2">
            <a href="{{ url('pelatihan/tambah_pelatihan') }}" class="btn btn-success btn-sm">Tambah Kegiatan</a>
        </div>
        @endif
    </div>
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama Kegiatan</th>
            <th>Kab/ Kota Kegiatan</th>
            <th>Provinsi</th>
            <th>Tanggal Pelaksanaan</th>
            <th>User</th>
            <th>Action</th>
            <th>Status/ Ket</th>
        </tr>
    </thead>
    <tfoot>
        <th>Jumlah</th>
        <th colspan="5">{{ $list_pelatihan->total() }} Kegiatan</th>
        
    </tfoot>
    <tbody>
        @php 
            $no = ($list_pelatihan->perPage() * $list_pelatihan->currentPage()) - ($list_pelatihan->perPage() - 1);
        @endphp
        @foreach($list_pelatihan as $pelatihan)
        <tr>
            <th style="width:3%;">
                {{ $no++ }}
                @if($pelatihan->status_pelatihan == 'Y')
                    <i class="pl-1 fa fa-lock fa-lg"></i>
                @endif
            </th>
            <td style="width:24%;"><a href="{{ url('pelatihan/list_data/'.$pelatihan->pelatihan_id) }}">{{ $pelatihan->nama_pelatihan }}</a></td>
            <th>{{ $pelatihan->kabupaten_kota }}</th>
            <th>{{ $pelatihan->provinsi }}</th>
            <th style="width:16.5%;">{{ strftime("%d %B %Y", strtotime($pelatihan->tanggal_mulai)) }} s.d {{ strftime("%d %B %Y", strtotime($pelatihan->tanggal_selesai)) }}</th>
            <th style="width:10%;">{{ $pelatihan->name }}</th>
            <td>
                @if($pelatihan->status_pelatihan == 'Y' && Auth::user()->user_level_id != 10)
                <button type="button" class="btn btn-sm btn-info dropdown-toggle" disabled>Action</button>
                @else
                <div class="btn-group">
                    <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu">
                        <a href="{{ url('pelatihan/edit_pelatihan/'.$pelatihan->pelatihan_id) }}" class="dropdown-item">Edit</a>
                        @if($pelatihan->status_pelatihan != 'Y')
                            <a href="{{ url('pelatihan/qrcode_and_lock/'.$pelatihan->pelatihan_id).'/'.$list_pelatihan->currentPage() }}" class="dropdown-item">QRCode dan Lock</a>
                        @endif
                        <a href="{{ url('pelatihan/delete_pelatihan/'.$pelatihan->pelatihan_id) }}" class="dropdown-item" onClick="return confirm('Apa Kamu Yakin Ingin Menghapusnya ?')">Delete</a>
                    </div>
                </div>
                @endif
            </td>

            <td>
                @php
                    if($pelatihan->status_Y != 0 && $pelatihan->status_Y == $pelatihan->jumlah_peserta) {
                        $status_keterangan = "SUDAH SEMUA";
                        $btn_color = "btn btn-success btn-sm";
                    } else if($pelatihan->status_Y > 0 && $pelatihan->status_Y < $pelatihan->jumlah_peserta) {
                        $status_keterangan = $pelatihan->status_Y." OF ".$pelatihan->jumlah_peserta;
                        $btn_color = "btn btn-warning btn-sm";
                    } else if($pelatihan->status_Y == 0 && $pelatihan->jumlah_peserta != 0) {
                        $status_keterangan = "BELUM SEMUA";
                        $btn_color = "btn btn-danger btn-sm";
                    } else if($pelatihan->jumlah_peserta == 0){
                        $status_keterangan = "PESERTA KOSONG";
                        $btn_color = "btn btn-secondary btn-sm";
                    } else {
                        $status_keterangan = "TIDAK ADA";
                        $btn_color = "";
                    }
                @endphp
                <button class="{{ $btn_color }}">{{ $status_keterangan }}</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="float-right">
    {{ $list_pelatihan->links() }}
</div>
@stop