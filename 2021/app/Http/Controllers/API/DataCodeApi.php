<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataCode;
use App\Http\Requests\DataCodeRequest;
use App\Pelatihan;

class DataCodeApi extends Controller
{
    public function store(Request $request)
    {
        $data_code = $request->data_code;
        $pelatihan_id = $request->pelatihan_id;
        $pelatihan = Pelatihan::find($pelatihan_id);
        $pelatihan->data_code2()->createMany($data_code);
        return response()->json(['alert' => 'success']);
    }
}
