<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Rekap Data Pelatihan</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('foto/logo_sertifikat_icon.ico') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> -->
    <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="{{ asset('css/styles/shards-dashboards.1.1.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles/extras.1.1.0.min.css') }}">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <style>
      .main-sidebar__nav-title {
        text-transform:uppercase;
        margin:0;
        font-size:.625rem;
        letter-spacing:.125rem;
        padding: 1px 1.5625rem;
        font-weight:500;
        color:#9ea8b9;
        border-bottom:1px solid #e1e5eb;
      }
      
    </style>
    @yield('style')
  </head>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <!-- <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="{{ asset('images/shards-dashboards-logo.svg') }}" alt="Shards Dashboard"> -->
                  <span class="d-none d-md-inline ml-1">
                    Rekap Data Pelatihan 2020
                  </span>
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <div class="nav-wrapper">
            <h6 class="main-sidebar__nav-title">Dashboard</h6>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ !empty($halaman) && $halaman == 'dashboard2' ? 'active' : '' }}" href="{{ url('dashboard2') }}">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard Utama</span>
                </a>
              </li>
            </ul>
            <h6 class="main-sidebar__nav-title">Menu</h6>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ !empty($halaman) && $halaman == 'profile_kegiatan_pelatihan' ? 'active' : '' }}" href="{{ route('profile_kegiatan_pelatihan') }}">
                  <i class="material-icons">dashboard</i>
                  <span>Profil Kegiatan Pelatihan</span>
                </a>
              </li>
            </ul>

            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ !empty($halaman) && $halaman == 'profile_peserta_pelatihan' ? 'active' : '' }}" href="{{ route('profile_peserta_pelatihan') }}">
                  <i class="material-icons">dashboard</i>
                  <span>Profil Peserta Pelatihan</span>
                </a>
              </li>
            </ul>

            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ !empty($halaman) && $halaman == 'profile_usaha_peserta' ? 'active' : '' }}" href="{{ route('profile_usaha_peserta') }}">
                  <i class="material-icons">dashboard</i>
                  <span>Profil Usaha Peserta Dilatih</span>
                </a>
              </li>
            </ul>

            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ !empty($halaman) && $halaman == 'penyebaran_peserta_pelatihan' ? 'active' : '' }}" href="{{ route('penyebaran_peserta_pelatihan') }}">
                  <i class="material-icons">dashboard</i>
                  <span>Penyebaran Peserta Pelatihan</span>
                </a>
              </li>
            </ul>
          </div>
        </aside>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              <div class="main-navbar__search w-100 d-none d-md-flex d-lg-flex"></div>
              <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle mr-2" src="{{ asset('images/avatars/admin.png') }}" alt="User Avatar">
                    <span class="d-none d-md-inline-block">{{ Auth::user()->name }}</span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons text-danger">&#xE879;</i> Logout </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
                </li>
              </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            @yield('content')
          </div>
          <footer class="main-footer d-flex p-2 px-3 bg-white border-top mt-3">
            <span class="copyright ml-auto my-auto mr-2">Copyright © @php echo date('Y'); @endphp
              <a href="https://designrevision.com" rel="nofollow">Deputi Pengembangan SDM</a>
            </span>
          </footer>
        </main>
      </div>
    </div>
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="{{ asset('js/scripts/extras.1.1.0.min.js') }}"></script>
    <script src="{{ asset('js/scripts/shards-dashboards.1.1.0.min.js') }}"></script>
    <script src="{{ asset('js/app-blog-overview.js') }}"></script>
    @yield('script')
    <!-- @include('appjs/app-blog-overview') -->
  </body>
</html>