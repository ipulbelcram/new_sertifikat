<table>
    <thead>
        <tr>
            <th colspan="14">LAPORAN DATA PESERTA KEGIATAN</th>
        </tr>
        <tr>
            <th colspan="14">{{ $data_user->name }}</th>
        </tr>
        <tr></tr><tr></tr>
            <tr>
                <th>Jenis Kegiatan</th>
                <th>{{ $pelatihan->jenis_pelatihan->params }}</th>
            </tr>
            <tr>
                <th>Nama Kegiatan</th>
                <th>{{ $pelatihan->nama_pelatihan }}</th>
            </tr>
            <tr>
                <th>Provinsi</th>
                <th>{{ $pelatihan->provinsi->provinsi }}</th>
            </tr>
            <tr>
                <th>Kab/ Kota Kegiatan</th>
                <th>{{ $pelatihan->kabupaten_kota->kab_kota }}</th>
            </tr>
            <tr>
                <th>Jumlah Peserta</th>
                <th>{{ $jumlah_data }}</th>
            </tr>
            <tr>
                <th>Tempat Pelaksanaan</th>
                <th>{{ $pelatihan->tempat }}</th>
            </tr>
            <tr>
                <th>Tanggal Pelaksanaan</th>
                <th>{{ date("d-m-Y", strtotime($pelatihan->tanggal_mulai)) }} s.d {{ date("d-m-Y", strtotime($pelatihan->tanggal_selesai)) }}</th>
            </tr>
            <tr></tr>
        <tr>
            <th>No</th>
            <th>Nama Lengkap</th>
            <th>Nomor KTP</th>
            <th>Status</th>
            <th>Jenis Kelamin</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Agama</th>
            <th>Pendidikan Terakhir</th>
            <th>Alamat Rumah</th>
            <th>Telp/ HP</th>
            <th>Status Usaha</th>
            <th>Nama Koperasi/ UMKM</th>
            <th>Alamat Koperasi/ UMKM</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no=1;
        @endphp
        @foreach($data as $item)
        @php 
            if($item->status == "L") {
                $status = "Lajang";
            } else {
                $status = "Menikah";
            }
        @endphp
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->nama }}</td>
                <td>'{{ $item->nik }}</td>
                <td>{{ $status }}</td>
                <td>{{ $item->jenis_kelamin }}</td>
                <td>{{ $item->tempat_lahir }}</td>
                <td>{{ date('d-m-Y', strtotime($item->tanggal_lahir)) }}</td>
                <td>{{ $item->agama }}</td>
                <td>{{ $item->pendidikan }}</td>
                <td>{{ $item->alamat }}</td>
                <td>{{ $item->no_telephone }}</td>
                <td>{{ $item->status_usaha }}</td>
                <td>{{ $item->nama_koperasi }} {{ $item->nama_umkm }}</td>
                <td>{{ $item->alamat_koperasi }} {{ $item->alamat_umkm }}</td>
            </tr>
        @endforeach
    </tbody>
</table>