@csrf
<div class="row">
        <div class="col-md-3">
            @if(isset($data_code->foto))
            <div id="uploaded_image"></div>
                @if(substr($data_code->foto, 0,4) == 'http')
                <img src="{{ $data_code->foto }}" id="photo" class="img-thumbnail"/>
                @else
                <img src="{{ asset('foto/'.$data_code->foto) }}" id="photo" class="img-thumbnail"/>
                @endif
            @else 
            <div id="uploaded_image"></div>
                <img src="{{ asset('foto/Male.jpg') }}" id="photo" class="img-thumbnail"/>
            @endif
            <h6>Upload a different photo...</h6>
                <div class="text-danger">
                    @if($errors->has('foto'))
                        {{ $errors->first('foto') }}
                    @endif
                </div>
            <label class="btn btn-primary">
                Browse&hellip; {{ Form::file('foto', ['id' => 'upload_image', 'accept' => 'image/*', 'style' =>'display: none']) }}
            </label>
        </div>

        <div class="col-md-9 rounded bg-light mb-3">
            <div class="form-row">
                <div class="col-md-12 pt-3">
                    <h4 class="text-center text-uppercase">Data Peserta</h4>
                    <hr>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold">Nama Lengkap</label>
                    {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
                    <div class="text-danger">
                        @if($errors->has('nama'))
                            {{ $errors->first('nama') }}
                        @endif
                    </div>
                </div>
                <div class=" form-group col-md-6">
                    <div id="image_name"></div>
                    {!! Form::hidden('pelatihan_id', $pelatihan_id) !!}
                    <label class="font-weight-bold">Nomor KTP</label>
                    {!! Form::text('nik', null, ['class' => 'form-control', 'placeholder' => 'Nomor KTP', 'maxlength' => '16',]) !!}
                    <div class="text-danger">
                        @if($errors->has('nik'))
                            {{ $errors->first('nik') }}
                        @endif
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold">Tempat Lahir</label>
                    {!! Form::text('tempat_lahir', null, ['class' => 'form-control', 'placeholder' => 'Tempat Lahir']) !!}
                    <div class="text-danger">
                        @if($errors->has('tempat_lahir'))
                            {{ $errors->first('tempat_lahir') }}
                        @endif
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold">Tanggal Lahir</label>
                    {!! Form::date('tanggal_lahir', !empty($data_code->tanggal_lahir) ? $data_code->tanggal_lahir->format('Y-m-d') : null, ['class' => 'form-control']) !!}
                    <div class="text-danger">
                        @if($errors->has('tanggal_lahir'))
                            {{ $errors->first('tanggal_lahir') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label class="font-weight-bold">Status Pernikahan</label>
                    {!! Form::select('status', ['L' => 'Belum Menikah', 'M' => 'Menikah'], null, ['class' => 'form-control', 'placeholder' => 'Status']) !!}
                    <div class="text-danger">
                        @if($errors->has('status'))
                            {{ $errors->first('status') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label class="font-weight-bold">Jenis Kelamin</label>
                    {!! Form::select('jenis_kelamin', ['L' => 'Laki-Laki', 'P' => 'Perempuan'], null, ['class' => 'form-control', 'placeholder' => 'Jenis Kelamin']) !!}
                    <div class="text-danger">
                        @if($errors->has('jenis_kelamin'))
                            {{ $errors->first('jenis_kelamin') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label class="font-weight-bold">Agama</label>
                    @if(!empty($list_agama))
                        {!! Form::select('agama_id', $list_agama, null, ['class' => 'form-control', 'placeholder' => 'Agama']) !!}
                    @else
                        <p>Tidak ada list Agama</p>
                    @endif
                    
                    <div class="text-danger">
                        @if($errors->has('agama_id'))
                            {{ $errors->first('agama_id') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label class="font-weight-bold">Pendidikan Terakhir</label>
                    {!! Form::select('pendidikan_terakhir_id', $list_pendidikan, null, ['class' => 'form-control', 'placeholder' => 'Pendidikan Terakhir']) !!}

                    <div class="text-danger">
                        @if($errors->has('pendidikan_terakhir_id'))
                            {{ $errors->first('pendidikan_terakhir_id') }}
                        @endif
                    </div>
                </div>

                <!-- <div class="form-group col-md-6">
                    <label class="font-weight-bold">Nomor NPWP</label>
                    {!! Form::text('npwp', null, ['class' => 'form-control', 'placeholder' => 'Nomor NPWP']) !!}

                    <div class="text-danger">
                        @if($errors->has('npwp'))
                            {{ $errors->first('npwp') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="font-weight-bold">Pekerjaan / Jabatan</label>
                    {!! Form::select('pekerjaan_jabatan', $list_pekerjaan_jabatan, null, ['class' => 'form-control', 'placeholder' => 'Pekerjaan / Jabatan']) !!}

                    <div class="text-danger">
                        @if($errors->has('pekerjaan_jabatan'))
                            {{ $errors->first('pekerjaan_jabatan') }}
                        @endif
                    </div>
                </div> -->
                <div class="form-group col-md-12">
                    <label class="font-weight-bold">Alamat Rumah</label>
                    {!! Form::textarea('alamat', null, ['class' => 'form-control', 'placeholder' => 'Alamat Rumah']) !!}

                    <div class="text-danger">
                        @if($errors->has('alamat'))
                            {{ $errors->first('alamat') }}
                        @endif
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold">Email</label>
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}

                    <div class="text-danger">
                        @if($errors->has('email'))
                            {{ $errors->first('email') }}
                        @endif
                    </div>
                </div>
                <div class=" form-group col-md-6">
                    <label class="font-weight-bold">Telp/HP</label>
                    {!! Form::number('no_telephone', null, ['class' => 'form-control', 'placeholder' => 'Telp/HP']) !!}

                    <div class="text-danger">
                        @if($errors->has('no_telephone'))
                            {{ $errors->first('no_telephone') }}
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <label class="font-weight-bold">Kabupaten/Kota Peserta</label>
                    {!! Form::text('kabupaten_kota', null, ['class' => 'form-control', 'placeholder' => 'Kabupaten / Kota Peserta']) !!}

                    <div class="text-danger">
                        @if($errors->has('kabupaten_kota'))
                            {{ $errors->first('kabupaten_kota') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="offset-md-3 col-md-9 mb-3 rounded bg-light">
            <div class="form-row">
                <div class="col-md-12 mt-3">
                    <h4 class="text-center text-uppercase">Data Koperasi atau UMKM Peserta</h4><hr>
                </div>

                <div class="form-group col-md-12">
                    <label class="font-weight-bold">Status Usaha</label>
                    {!! Form::select('status_usaha_id', $status_usaha, null, ['class' => 'form-control', 'placeholder' => 'Status Usaha', 'id' => 'status_usaha']) !!}

                    <div class="text-danger">
                        @if($errors->has('status_usaha_id'))
                            {{ $errors->first('status_usaha_id') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="offset-md-3 col-md-9 mb-3 rounded bg-light" id="data_umkm">
            <div class="form-row">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h4 class="text-center text-uppercase">Data Usaha(UMKM)</h4><hr>
                    </div>
                    <!-- <div class="form-group col-md-12">
                        <label class="font-weight-bold">Sektor Usaha Anda</label>
                        {!! Form::select('rencana_usaha', $list_bidang_usaha, null, ['class' => 'form-control', 'placeholder' => 'Sektor Usaha Anda']) !!}
                        
                        <div class="text-danger">
                            @if($errors->has('rencana_usaha'))
                                {{ $errors->first('rencana_usaha') }}
                            @endif
                        </div>
                    </div> -->

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Nama Usaha</label>
                        {!! Form::text('nama_umkm', null, ['class' => 'form-control', 'placeholder' => 'Nama Usaha']) !!}
                        <div class="text-danger">
                            @if($errors->has('nama_umkm'))
                                {{ $errors->first('nama_umkm') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Alamat Usaha</label>
                        {!! Form::textarea('alamat_umkm', null, ['class' => 'form-control', 'placeholder' => 'Alamat Usaha', 'rows' => '6']) !!}
                        <div class="text-danger">
                            @if($errors->has('alamat_umkm'))
                                {{ $errors->first('alamat_umkm') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Nomor Induk Berusaha (NIB)</label>
                        {!! Form::text('nib', null, ['class' => 'form-control', 'placeholder' => 'Nomor Induk Berusaha (NIB)']) !!}
                        <div class="text-danger">
                            @if($errors->has('nib'))
                                {{ $errors->first('nib') }}
                            @endif
                        </div>
                    </div>
                        
                    <!-- <div class="form-group col-md-6">
                        <label class="font-weight-bold">Jenis Usaha</label>
                        {!! Form::select('jenis_usaha_id', $jenis_usaha, null, ['class' => 'form-control', 'id' => 'jenis_usaha', 'placeholder' => 'Pilih Jenis Usaha']) !!}
                        <div class="text-danger">
                            @if($errors->has('jenis_usaha_id'))
                                {{ $errors->first('jenis_usaha_id') }}
                            @endif
                        </div>
                    </div> -->
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Bidang Usaha</label>
                        <!-- <input type="hidden" id="val_bidang_usaha" value="{{ !empty($data_code) ? $data_code->bidang_usaha_id :'' }}"> -->
                        {!! Form::select('bidang_usaha_id', $list_bidang_usaha, null, ['class' => 'form-control', 'id' => 'bidang_usaha', 'placeholder' => 'Pilih Bidang Usaha']) !!}
                        <div class="text-danger">
                            @if($errors->has('bidang_usaha_id'))
                                {{ $errors->first('bidang_usaha_id') }}
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <label class="font-weight-bold"> Lama Usaha</label>
                        <div class="form-group">
                            {!! Form::select('lama_usaha', ['< 6 Bulan' => '< 6 Bulan', '6 Bulan - 2 Tahun' => '6 Bulan - 2 Tahun', '> 2 Tahun' => '> 2 Tahun'], null, ['class' => 'form-control', 'placeholder' => 'Lama Usaha UMKM']) !!}
                            <div class="text-danger">
                                @if($errors->has('lama_usaha'))
                                    {{ $errors->first('lama_usaha') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Jumlah Tenaga Kerja UMKM</label>
                        {!! Form::text('tenaga_kerja_umkm', null, ['class' => 'form-control number', 'placeholder' => 'Jumlah Tenaga Kerja UMKM']) !!}
                        <div class="text-danger">
                            @if($errors->has('tenaga_kerja_umkm'))
                                {{ $errors->first('tenaga_kerja_umkm') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Jumlah Omzet per Bulan</label>
                        {!! Form::text('omset_usaha_perbulan', null, ['class' => 'form-control price', 'placeholder' => 'Omset Usaha per Bulan']) !!}
                        <div class="text-danger">
                            @if($errors->has('omset_usaha_perbulan'))
                                {{ $errors->first('omset_usaha_perbulan') }}
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="font-weight-bold"> Rentang Omzet per Bulan</label>
                        <div class="form-group">
                            {!! Form::select('rentang_omset', [
                                '< 5 Juta' => '< 5 Juta', 
                                '5 - 25 Juta' => '5 - 25 Juta', 
                                '25 - 50 Juta' => '25 - 50 Juta', 
                                '50 - 100 Juta' => '50 - 100 Juta', 
                                '100 - 160 Juta' => '100 - 160 Juta'
                            ], null, ['class' => 'form-control', 'placeholder' => 'Rentang Omzet per Bulan']) !!}
                            <div class="text-danger">
                                @if($errors->has('rentang_omset'))
                                    {{ $errors->first('rentang_omset') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="offset-md-3 col-md-9 mb-3 rounded bg-light" id="data_koperasi">
            <div class="form-row">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h4 class="text-center text-uppercase">Data Koperasi</h4><hr>
                    </div>
                    <!-- <div class="form-group col-md-12">
                        <label class="font-weight-bold">Sektor Usaha Koperasi Anda</label>
                        {!! Form::select('bidang_usaha_koperasi_id', $bidang_usaha_koperasi, null, ['class' => 'form-control', 'placeholder' => 'Sektor Usaha Koperasi Anda']) !!}
                        <div class="text-danger">
                            @if($errors->has('bidang_usaha_koperasi_id'))
                                {{ $errors->first('bidang_usaha_koperasi_id') }}
                            @endif
                        </div>
                    </div> -->
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Nama Koperasi</label>
                        {!! Form::text('asal_lembaga', null, ['class' => 'form-control', 'placeholder' => 'Nama Koperasi']) !!}
                        <div class="text-danger">
                            @if($errors->has('asal_lembaga'))
                                {{ $errors->first('asal_lembaga') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Alamat Koperasi</label>
                        {!! Form::textarea('alamat_koperasi', null, ['class' => 'form-control', 'placeholder' => 'Alamat Koperasi', 'rows' => '6']) !!}
                        <div class="text-danger">
                            @if($errors->has('alamat_koperasi'))
                                {{ $errors->first('alamat_koperasi') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Nomor Induk Koperasi (NIK)</label>
                        {!! Form::select('nik_koperasi', ['true' => 'Ada', 'false' => 'Tidak Ada'], null, ['class' => 'form-control', 'placeholder' => 'Nomor Induk Koperasi (NIK)']) !!}
                        <div class="text-danger">
                            @if($errors->has('nik_koperasi'))
                                {{ $errors->first('nik_koperasi') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Jenis Usaha Koperasi</label>
                        {!! Form::select('bidang_koperasi_id',$list_bidang_koperasi, null, ['class' => 'form-control', 'placeholder' => 'Jenis Usaha Koperasi']) !!}
                        <div class="text-danger">
                            @if($errors->has('bidang_koperasi_id'))
                                {{ $errors->first('bidang_koperasi_id') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Jumlah Tenaga Kerja Koperasi</label>
                        {!! Form::text('tenaga_kerja_koperasi', null, ['class' => 'form-control number', 'placeholder' => 'Jumlah Tenaga Kerja Koperasi']) !!}
                        <div class="text-danger">
                            @if($errors->has('tenaga_kerja_koperasi'))
                                {{ $errors->first('tenaga_kerja_koperasi') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Omset Koperasi per Tahun</label>
                        {!! Form::text('omset_koperasi', null, ['class' => 'form-control price', 'placeholder' => 'Omset Koperasi per Tahun']) !!}
                        <div class="text-danger">
                            @if($errors->has('omset_koperasi'))
                                {{ $errors->first('omset_koperasi') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @if($user_create_id == 4)
        <div class="offset-md-3 col-md-9 mb-3 rounded bg-light" id="data_mikro">
            <div class="form-row">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h4 class="text-center text-uppercase">Tambahan Informasi Khusus</h4><hr>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Produk</label>
                        {!! Form::text('product', null, ['class' => 'form-control', 'placeholder' => 'Contoh: olahan susu, kopi dan lainnya']) !!}
                        <div class="text-danger">
                            @if($errors->has('product'))
                                {{ $errors->first('product') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Merek Produk</label>
                        {!! Form::text('merek_product', null, ['class' => 'form-control', 'placeholder' => 'Merek Produk']) !!}
                        <div class="text-danger">
                            @if($errors->has('merek_product'))
                                {{ $errors->first('merek_product') }}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="font-weight-bold"> Sertifikat Produk</label>
                        <div class="form-group">
                            {!! Form::select('sertifikat_product', [
                                'Merek' => 'Merek', 
                                'Hak Cipta' => 'Hak Cipta', 
                                'PIRT' => 'PIRT', 
                                'Halal' => 'Halal', 
                                'BPOM' => 'BPOM', 
                                'Lainnya' => 'Lainnya', 
                                'Belum Ada' => 'Belum Ada', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Sertifikat Produk']) !!}
                            <div class="text-danger">
                                @if($errors->has('sertifikat_product'))
                                    {{ $errors->first('sertifikat_product') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="font-weight-bold"> Kemitraan</label>
                        <div class="form-group">
                            {!! Form::select('kemitraan', [
                                'Ya' => 'Ya', 
                                'Tidak' => 'Tidak', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Kemitraan']) !!}
                            <div class="text-danger">
                                @if($errors->has('kemitraan'))
                                    {{ $errors->first('kemitraan') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="font-weight-bold"> Kredit Perbankan</label>
                        <div class="form-group">
                            {!! Form::select('kredit_perbankan', [
                                'Ya' => 'Ya', 
                                'Tidak Ada' => 'Tidak Ada', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Kredit Perbankan']) !!}
                            <div class="text-danger">
                                @if($errors->has('kredit_perbankan'))
                                    {{ $errors->first('kredit_perbankan') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="font-weight-bold"> Tergabung Dalam Asosiasi/Kop</label>
                        <div class="form-group">
                            {!! Form::select('tergabung_asosiasi', [
                                'Ya' => 'Ya', 
                                'Tidak' => 'Tidak', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Tergabung Dalam Asosiasi/Kop']) !!}
                            <div class="text-danger">
                                @if($errors->has('tergabung_asosiasi'))
                                    {{ $errors->first('tergabung_asosiasi') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="font-weight-bold"> Akun Sosial Media Usaha</label>
                        <div class="form-group form-check">
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Instagram',  (!empty($sosmed) && in_array('Instagram', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Instagram</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Facebook',  (!empty($sosmed) && in_array('Facebook', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Facebook</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Twitter',  (!empty($sosmed) && in_array('Twitter', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Twitter</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Whatsapp',  (!empty($sosmed) && in_array('Whatsapp', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Whatsapp</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Telegram',  (!empty($sosmed) && in_array('Telegram', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Telegram</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Youtube',  (!empty($sosmed) && in_array('Youtube', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Youtube</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Tiktok',  (!empty($sosmed) && in_array('Tiktok', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Tiktok</label>
                            </div>
                            <div>
                                {!! Form::checkbox('sosmed[]', 'Belum Ada',  (!empty($sosmed) && in_array('Belum Ada', $sosmed)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Belum Ada</label>
                            </div>
                            <div class="text-danger">
                                @if($errors->has('sosmed'))
                                    {{ $errors->first('sosmed') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Nama Akun Marketplace</label>
                        {!! Form::text('nama_marketplace', null, ['class' => 'form-control', 'placeholder' => 'Nama Akun Marketplace']) !!}
                        <div class="text-danger">
                            @if($errors->has('nama_marketplace'))
                                {{ $errors->first('nama_marketplace') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Akun Marketplace</label>
                        <div class="form-group form-check">
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Tokopedia',  (!empty($akun_marketplace) && in_array('Tokopedia', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Tokopedia</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Shopee',  (!empty($akun_marketplace) && in_array('Shopee', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Shopee</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Lazada',  (!empty($akun_marketplace) && in_array('Lazada', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Lazada</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Bukalapak',  (!empty($akun_marketplace) && in_array('Bukalapak', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Bukalapak</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Blibli',  (!empty($akun_marketplace) && in_array('Blibli', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Blibli</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Website/Blogspot',  (!empty($akun_marketplace) && in_array('Website/Blogspot', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Website/Blogspot</label>
                            </div>
                            <div>
                                {!! Form::checkbox('akun_marketplace[]', 'Belum Ada',  (!empty($akun_marketplace) && in_array('Belum Ada', $akun_marketplace)) ? true : false, ['class' => 'form-check-input']) !!}
                                <label class="form-check-label" for="exampleCheck1">Belum Ada</label>
                            </div>
                            <div class="text-danger">
                                @if($errors->has('akun_marketplace'))
                                    {{ $errors->first('akun_marketplace') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="font-weight-bold"> Jangkauan Pemasaran</label>
                        <div class="form-group">
                            {!! Form::select('jangkauan_pemasaran', [
                                'Lokal' => 'Lokal', 
                                'Ekspor' => 'Ekspor', 
                                'Lintas Provinsi' => 'Lintas Provinsi', 
                                'Lintas Kab/Kota' => 'Lintas Kab/Kota', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Jangkauan Pemasaran']) !!}
                            <div class="text-danger">
                                @if($errors->has('jangkauan_pemasaran'))
                                    {{ $errors->first('jangkauan_pemasaran') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="font-weight-bold"> Rentang Modal</label>
                        <div class="form-group">
                            {!! Form::select('rentang_modal', [
                                '< 5 Juta' => '< 5 Juta', 
                                '5 - 25 Juta' => '5 - 25 Juta', 
                                '25 - 100 Juta' => '25 - 100 Juta', 
                                '100 - 500 Juta' => '100 - 500 Juta', 
                                '500 - 1 Miliar' => '500 - 1 Miliar', 
                            ], null, ['class' => 'form-control', 'placeholder' => 'Rentang Modal']) !!}
                            <div class="text-danger">
                                @if($errors->has('rentang_modal'))
                                    {{ $errors->first('rentang_modal') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="font-weight-bold">Modal Usaha</label>
                        {!! Form::text('modal_usaha', null, ['class' => 'form-control price', 'placeholder' => 'Modal Usaha']) !!}
                        <div class="text-danger">
                            @if($errors->has('modal_usaha'))
                                {{ $errors->first('modal_usaha') }}
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="font-weight-bold"> Kendala Utama</label>
                        <div class="form-group">
                            {!! Form::select('kendala_utama_id', $list_kendala, null, ['class' => 'form-control', 'placeholder' => 'Kendala Utama']) !!}
                            <div class="text-danger">
                                @if($errors->has('kendala_utama_id'))
                                    {{ $errors->first('kendala_utama_id') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="offset-md-3 col-md-9 mb-3 px-0 mt-3">
            {!! Form::submit($button, ['class' => 'btn btn-primary btn-block']) !!}
        </div>
    </div>