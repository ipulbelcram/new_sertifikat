$(document).ready(function() {
  var status_usaha = $("#status_usaha").val();

  if (status_usaha == 25) {
    $("#data_umkm").hide();
    $("#data_koperasi").show();
  } else if (status_usaha == 26) {
    $("#data_umkm").show();
    $("#data_koperasi").hide();
  } else {
    $("#data_umkm").hide();
    $("#data_koperasi").hide();
  }

  $("#status_usaha").on("change", function() {
    var status_usaha = $(this).val();
    if (status_usaha == 25) {
      $("#data_umkm").hide();
      $("#data_koperasi").show();
    } else if (status_usaha == 26) {
      $("#data_umkm").show();
      $("#data_koperasi").hide();
    } else {
      $("#data_umkm").hide();
      $("#data_koperasi").hide();
    }
  });

  $("#jenis_usaha option:first-child").attr("disabled", true);
  $("#bidang_usaha option:first-child").attr("disabled", true);

  var jenis_usaha_id = $("#jenis_usaha").val();
  if (jenis_usaha_id != null) {
    $.ajax({
      url: "http://diklatsdm.kemenkopukm.go.id/2020/api/bidang_usaha",
      type: "GET",
      dataType: "JSON",
      data: {
        jenis_usaha_id: jenis_usaha_id
      },
      success: function(result) {
        var data = result.data;
        $.each(data, function(key, value) {
          $("#bidang_usaha").append(
            "<option value=" + value.id + ">" + value.bidang_usaha + "</option>"
          );
        });
        var val_bidang_usaha = $("#val_bidang_usaha").val();
        $("#bidang_usaha option[value=" + val_bidang_usaha + "]").attr(
          "selected",
          true
        );
      }
    });
  } else {
    $("#bidang_usaha").attr("disabled", true);
  }

  $("#jenis_usaha").on("change", function() {
    $("#bidang_usaha").html("");
    var jenis_usaha_id = $(this).val();
    $.ajax({
      url: "http://diklatsdm.kemenkopukm.go.id/2020/api/bidang_usaha",
      type: "GET",
      dataType: "JSON",
      data: {
        jenis_usaha_id: jenis_usaha_id
      },
      success: function(result) {
        var data = result.data;
        $.each(data, function(key, value) {
          $("#bidang_usaha").append(
            "<option value=" + value.id + ">" + value.bidang_usaha + "</option>"
          );
        });
        $("#bidang_usaha").attr("disabled", false);
      }
    });
  });
});
