@extends('template')
@section('content')
    {!! Form::open(['url' => 'pelatihan']) !!}
        @include('pelatihan/form')
    {!! Form::close() !!}
    @endSection
@section('script')
    <script src="{{ asset('js/pelatihanForm.js') }}"></script>
@endSection