<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidangKoperasi extends Model
{
    protected $table = 'bidang_koperasi';

    protected $fillable = [
        'bidang_koperasi',
    ];
}
