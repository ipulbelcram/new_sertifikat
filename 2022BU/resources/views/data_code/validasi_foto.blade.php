@extends('template')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        @php
            $nama_pelatihan = $pelatihan->nama_pelatihan;
            $tempat_pelatihan = $pelatihan->tempat;
            $tanggal_mulai = $pelatihan->tanggal_mulai;
            $tanggal_selesai = $pelatihan->tanggal_selesai;
            $kabupaten_kota_pelatihan = $pelatihan->kabupaten_kota->kab_kota;
            $provinsi_pelatihan = $pelatihan->provinsi->provinsi;
            $pelatihan_id = $pelatihan->id;
            $status_pelatihan = $pelatihan->status;
            $no = ($data_code->perPage() * $data_code->currentPage()) - ($data_code->perPage() - 1);
        @endphp
        <h1 class="h2">VALIDASI FOTO {{ $nama_pelatihan }}</h1>
    </div>
    <div>
        <table>
            <tr>
                <td><strong>Tempat/ Hotel Kegiatan</strong></td>
                <td class="px-4">:</td>
                <td>{{ $tempat_pelatihan }}</td>
            </tr>
            <tr>
                <td><strong>Tanggal Pelaksanaan</strong></td>
                <td class="px-4">:</td>
                <td>{{ strftime("%d %B %Y", strtotime($tanggal_mulai)) }} s.d {{ strftime("%d %B %Y", strtotime($tanggal_selesai)) }}</td>
            </tr>

            <tr>
                <td><strong>Kab/ Kota Kegiatan</strong></td>
                <td class="px-4">:</td>
                <td>{{ $kabupaten_kota_pelatihan }}</td>
            </tr>

            <tr>
                <td><strong>Provinsi</strong></td>
                <td class="px-4">:</td>
                <td>{{ $provinsi_pelatihan }}</td>
            </tr>
        </table>
    </div>
    <br>
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="text-center" style="width:5%">No</th>
                <th class="text-center" style="width:30%">Nama</th>
                <th class="text-center" style="width:30%">Telp/ HP</th>
                <th class="text-center">Foto</th>
            </tr>
        </thead>
        <tfoot>
            <th>Jumlah</th>
            <th>{{ $data_code->total() }} Peserta</th>
        </tfoot>
        <tbody>
            @foreach($data_code as $data)
                <tr>   
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nama }}</td>
                    @php
                        if(!empty($data->no_telephone)){
                            $no_telp = $data->no_telephone;
                        } else {
                            $no_telp = "-";
                        }
                    @endphp
                    <td class="text-center">{{ $no_telp }}</td>
                    <td class="text-center">
                        
                        @if(!empty($data->foto))
                            <?php $foto_link = substr($data->foto, 0, 4) ?>
                            @if($foto_link == 'http')
                            <img src="{{ $data->foto }}" class="rounded mx-auto d-block" alt="..." width="40%">
                            @else
                            <img src="{{ asset('foto/'.$data->foto) }}" class="rounded mx-auto d-block" alt="..." width="40%">
                            @endif
                        @else 
                            Tidak ada foto
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $data_code->links() }}
    </div>
@stop