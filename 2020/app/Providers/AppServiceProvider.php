<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'id_ID.utf8');

        $halaman = "Kosong";
        if(Request::segment(2) == "1") {
            $halaman = "1";
        }

        if(Request::segment(2) == "2") {
            $halaman = "2";
        }

        if(Request::segment(2) == "3") {
            $halaman = "3";
        }

        if(Request::segment(2) == "4") {
            $halaman = "4";
        }

        if(Request::segment(2) == "5") {
            $halaman = "5";
        }

        if(Request::segment(2) == "6") {
            $halaman = "6";
        }

        if(Request::segment(2) == "7") {
            $halaman = "7";
        }

        if(Request::segment(1) == "dashboard2") {
            $halaman = "dashboard2";
        }

        if(Request::segment(1) == "profile_kegiatan_pelatihan") {
            $halaman = "profile_kegiatan_pelatihan";
        }

        if(Request::segment(1) == "profile_peserta_pelatihan") {
            $halaman = "profile_peserta_pelatihan";
        }

        if(Request::segment(1) == "profile_usaha_peserta") {
            $halaman = "profile_usaha_peserta";
        }

        if(Request::segment(1) == "penyebaran_peserta_pelatihan") {
            $halaman = "penyebaran_peserta_pelatihan";
        }
        view()->share('halaman', $halaman);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
