<?php

namespace App\Http\Controllers;

use App\CodePelatihan;
use Illuminate\Http\Request;
use App\Pelatihan;
use App\Provinsi;
use App\DataCode;
use App\User;
use App\Params;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PelatihanRequest;

class PelatihanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('administrator', ['only' => [
            'list_pelatihan',
            'unlock_status_pelatihan',
            'update_status_pelatihan',
        ]]);
        $this->middleware('notAdminStatistik', ['except' => [
            'list_pelatihan',
            'unlock_status_pelatihan',
            'update_status_pelatihan',
        ]]);
    }

    public function index(Request $request)
    {
        $code_user = Auth::user()->code_user;
        $filterby = $request->input('filterby');
        if($code_user == 'Z') {
            if(!empty($filterby)){
                $list_pelatihan = DB::table('vw_ket_pelatihan')->where([
                    ['filterby', '=', $filterby],
                ])->orderBy('tanggal_mulai', 'desc')->paginate(10);    
            } else {
                $list_pelatihan = DB::table('vw_ket_pelatihan')->orderBy('tanggal_mulai', 'desc')->paginate(10);
            }
            $list_users = User::where('user_level_id', 102)->orderBy('order','ASC')->get(['id','code_user', 'name']);
        } else {
            if(!empty($filterby)) {
                $list_pelatihan = DB::table('vw_ket_pelatihan')->where([
                    ['user_id', '=', Auth::id()],
                    ['filterby', '=', $filterby],
                ])->orderBy('tanggal_mulai', 'desc')->paginate(10);    
            } else {
                $list_pelatihan = DB::table('vw_ket_pelatihan')->where('user_id', Auth::id())->orderBy('tanggal_mulai', 'desc')->paginate(10);
            }
            $list_users ="";
        }
        if(Auth::user()->user_level_id == '10') {
            $total_data_terbarcode = DB::table('vw_ket_code_pelatihan')->where('status', 'Y')->count();
            $jumlah_data_code = DataCode::all()->count();
        } else {
            $total_data_terbarcode = DB::table('vw_ket_code_pelatihan')->where([
                ['status', 'Y'],
                ['id_user', Auth::user()->id],
            ])->count();
            $jumlah_data_code = DB::table('vw_all_data_code')->where('user_id', Auth::user()->id)->count();
        }

        $list_pelatihan->appends(['filterby' => $filterby]);
        $id_user = 0;

        return view('pelatihan/index', ['list_pelatihan' => $list_pelatihan, 'list_users' => $list_users, 'id_user' => $id_user, 'total_data_terbarcode' => $total_data_terbarcode, 'jumlah_data_code' => $jumlah_data_code]);
    }

    public function list_pelatihan(Request $request, $user_id)
    {   
        $filterby = $request->input('filterby');
        if(!empty($filterby)) {
            $list_pelatihan = DB::table('vw_ket_pelatihan')->where([
                ['user_id', '=', $user_id],
                ['filterby', '=', $filterby ],
            ])->orderBy('tanggal_mulai', 'desc')->paginate(10);
        } else {
            $list_pelatihan = DB::table('vw_ket_pelatihan')->where('user_id', $user_id)->orderBy('tanggal_mulai', 'desc')->paginate(10);
        }
        $list_pelatihan->appends(['filterby' => $filterby]);
        $list_users = User::where('user_level_id', 102)->orderBy('order','ASC')->get(['id','code_user', 'name']);
        $jumlah_pelatihan = Pelatihan::where('user_id', $user_id)->count();
        $id_user = $user_id;
        $total_data_terbarcode = DB::table('vw_ket_code_pelatihan')->where([
            ['status', '=', 'Y'],
            ['id_user', '=', $user_id]
        ])->count();
        $jumlah_data_code = DB::table('vw_all_data_code')->where('user_id', $user_id)->count();
        return view('pelatihan/index', ['list_pelatihan' => $list_pelatihan, 'list_users' => $list_users, 'jumlah_pelatihan' => $jumlah_pelatihan, 'id_user' => $id_user, 'total_data_terbarcode' => $total_data_terbarcode, 'jumlah_data_code' => $jumlah_data_code]);
    }

    public function tambah() 
    {
        $button = "Create";
        // if(Auth::user()->id == 5) {
        //     $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true'], ['level', Auth::user()->id]])->pluck('params', 'id');
        // } else {
        //     $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true'], ['level', Auth::user()->id]])->orWhere([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])->whereNull('level')->pluck('params', 'id');
        // }
        $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])->pluck('params', 'id');
        $list_provinsi = Provinsi::pluck('provinsi', 'id');
        $list_dalam_rangka = Params::where([['category_params', 'dalam_rangka'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        $list_dukungan_sektor = Params::where([['category_params', 'dukungan_sektor'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        $list_metode_pelaksanaan = Params::where([['category_params', 'metode_pelaksanaan'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        return view('pelatihan/tambahPelatihan', compact('button', 'list_provinsi', 'jenis_pelatihan', 'list_dalam_rangka', 'list_dukungan_sektor', 'list_metode_pelaksanaan'));
    }

    public function store( PelatihanRequest $request)
    {
        $input = $request->all();

        if($request->metode_pelaksanaan_id == 153) {
            $input['nama_perusahaan'] = '';
            $input['direktur_utama'] = '';
            $input['no_hp'] = '';
            $input['penanggung_jawab_kontraktual'] = '';
            $input['pelaksana_kontraktual'] = '';
            $input['no_hp_pelaksana_kontraktual'] = '';
        } else {
            $input['penanggung_jawab_swakelola'] = '';
            $input['pelaksana_swakelola'] = '';
            $input['no_hp_pelaksana_swakelola'] = '';
        }
        Pelatihan::create($input);
        return redirect('/dashboard');
        
    }

    public function edit($id)
    {
        $data_pelatihan = Pelatihan::findOrFail($id);
        $button = "Simpan";
        if(Auth::user()->user_level_id == 10 ) {
            $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])
                                ->orWhere([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])->whereNull('level')->pluck('params', 'id');
        } else {
            // if(Auth::user()->id == 5) {
            //     $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true'], ['level', Auth::user()->id]])->pluck('params', 'id');
            // } else {
            //     $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true'], ['level', Auth::user()->id]])
            //                         ->orWhere([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])->whereNull('level')->pluck('params', 'id');
            // }
            $jenis_pelatihan = Params::where([['category_params', 'jenis_pelatihan_kop'], ['active', 'true']])->pluck('params', 'id');
        }
        $list_provinsi = Provinsi::pluck('provinsi', 'id');
        $list_dalam_rangka = Params::where([['category_params', 'dalam_rangka'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        $list_dukungan_sektor = Params::where([['category_params', 'dukungan_sektor'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        $list_metode_pelaksanaan = Params::where([['category_params', 'metode_pelaksanaan'], ['active', 'true']])->orderBy('order', 'ASC')->pluck('params', 'id');
        return view('pelatihan/edit_pelatihan', compact('data_pelatihan', 'button', 'list_provinsi', 'jenis_pelatihan', 'list_dalam_rangka', 'list_dukungan_sektor', 'list_metode_pelaksanaan'));
    }

    public function update($id, PelatihanRequest $request)
    {
        $input = $request->all();
        $pelatihan = Pelatihan::findOrFail($id);

        if($request->metode_pelaksanaan_id == 153) {
            $input['nama_perusahaan'] = '';
            $input['direktur_utama'] = '';
            $input['no_hp'] = '';
            $input['penanggung_jawab_kontraktual'] = '';
            $input['pelaksana_kontraktual'] = '';
            $input['no_hp_pelaksana_kontraktual'] = '';
        } else {
            $input['penanggung_jawab_swakelola'] = '';
            $input['pelaksana_swakelola'] = '';
            $input['no_hp_pelaksana_swakelola'] = '';
        }
        
        $pelatihan->update($input);
        return redirect('/dashboard');
    }

    public function destroy($pelatihan_id)
    {
        $pelatihan = Pelatihan::find($pelatihan_id);
        $pelatihan->delete();
        return redirect('dashboard');
    }

    public function search(Request $request) 
    {
        $code_user = Auth::user()->code_user;
        $cari_pelatihan = $request->cari_pelatihan;
        if($code_user == "Z") {
            $list_users = User::where('user_level_id', 102)->get(['id','code_user', 'name']);
            $list_pelatihan = DB::table('vw_ket_pelatihan')->where('nama_pelatihan','like',"%".$cari_pelatihan."%")->orderBy('tanggal_mulai', 'DESC')->paginate();
        } else {
            $list_users = "";
            $list_pelatihan = DB::table('vw_ket_pelatihan')->where([
                ['nama_pelatihan','like',"%".$cari_pelatihan."%"],
                ['user_id', '=', Auth::user()->id],
            ])->orderBY('tanggal_mulai', 'DESC')->paginate();
        }
        $list_pelatihan->appends(['cari_pelatihan' => $cari_pelatihan]);
        $id_user = 0;
        return view('pelatihan/index',['list_pelatihan' => $list_pelatihan, 'list_users' => $list_users, 'id_user' => $id_user]);
    }

    public function update_status_pelatihan($pelatihan_id)
    {
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $input['status'] = "Y";
        $pelatihan->update($input);
        return redirect()->route('data_peserta_pelatihan', [$pelatihan_id]);
    }

    public function unlock_status_pelatihan($pelatihan_id)
    {
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $input['status'] = "N";
        $pelatihan->update($input);
        return redirect()->route('data_peserta_pelatihan', [$pelatihan_id]);
    }

    public function qrcodeAndLock($pelatihan_id, $currentpage)
    {
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $data_codes = DataCode::where('pelatihan_id', $pelatihan->id)->get();

        foreach ($data_codes as $data_code) {
            $tanggal_sekarang = date('Y-m-d');
            $code_pelatihan = CodePelatihan::where('tanggal_input', $tanggal_sekarang)->select('no_urut')->max('no_urut');
            if(empty($code_pelatihan)) {
                $no_urut = 1;
            } else {
                $no_urut = $code_pelatihan+1;
            }

            $data_code2 = DataCode::findOrFail($data_code->id);
            $code_user =  $data_code2->pelatihan[0]->user->code_user;
            $input['data_code_id'] = $data_code2->id;
            $input['code_user'] = $code_user;
            $input['tanggal_input'] = $tanggal_sekarang;
            $input['no_urut'] = $no_urut;
            $input['status'] = "Y";
            $cek_code = CodePelatihan::where('data_code_id', $data_code->id)->count();
            if($cek_code < 1) {
                CodePelatihan::create($input);
            }
        }

        $inputPelatihan['status'] = "Y";
        $pelatihan->update($inputPelatihan);
        return redirect()->route('dashboard', ['page' => $currentpage]);
    }
}
