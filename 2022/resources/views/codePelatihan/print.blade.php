<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="{{ asset('foto/logo_sertifikat_icon.ico') }}">
        <title>Print Sertifikat QrCode</title>
        <link rel="icon" href="{{ asset('foto/logo_sertifikat_icon.ico') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('vendors/bootstrap-4.2.1/css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
    </head>
    <body>
    @foreach($data_dataCode as $dataCode)
    <br><br><br><br><br><br><br><br><br><br><br><br>
        <div class="text-center col-lg-7 d-block mx-auto">
            @if($tanggal_status == 'F')
                <div style=margin-top:40px;>
                    @if(substr($dataCode->foto, 0, 4) == 'http')
                    <img class="foto d-block mx-auto" src="{{ $dataCode->foto }}" alt="" style="border:none !important">
                    @else
                    <img class="foto d-block mx-auto" src="{{ asset('foto/'.$dataCode->foto) }}" alt="" style="border:none !important">
                    @endif
                </div><br>
            @else 
                <div class="foto d-block mx-auto " style="margin-top:40px; width:60px !important; height:80px !important;"></div><br>
            @endif
            <div class="tahoma" style=margin-top:-20px;>Diberikan Kepada :</div> 
            <div class="vivaldi" style=margin-top:-20px;>{{ $dataCode->nama }}</div>
            <div class="tahoma" style=margin-top:-20px;>Telah Mengikuti :</div>
            <div class="tahoma font-pelatihan upercase">{{ $dataCode->pelatihan[0]->nama_pelatihan }}</div>
            @if($tanggal_status == 'N')
            @else
                <div class="tahoma">Pada Tanggal {{ strftime("%d %B", strtotime($dataCode->pelatihan[0]->tanggal_mulai->format('d F'))) }} s.d {{ strftime("%d %B %Y", strtotime($dataCode->pelatihan[0]->tanggal_selesai->format('d F Y'))) }}</div>
            @endif
            <div class="tahoma">Di <span class="text-capitalize">{{ $dataCode->pelatihan[0]->kabupaten_kota->kab_kota }}</span> Provinsi {{ $dataCode->pelatihan[0]->provinsi->provinsi }} </div>
            <div class="tahoma">Diselenggarakan oleh :</div>
            <div class="tahoma">{{ $dataCode->pelatihan[0]->user->ttd->unit }}</div>
        </div>
        <br>
        <div class="float-left">
            @php
                $link = url("/pelatihan/detail_data_code/".$QrCode);
            @endphp
            <div class="code text-center">
                <div class="qrcode">{!! QrCode::size(179)->generate($link) !!}</div>
                <!-- <div class="tahoma font-code">{{ $QrCode }}</div> -->
                <div>
                    <div style="margin-top:-5;"><small>Sertifikat Sudah Diotorisasi</small></div>
                    <div style="margin-top:-5;"><small>Secara Elektronik</small></div>
                </div>
            </div>
        </div>
        <div class="float-right ttd-margin" style="margin-right: 200px;">
            <div class="col-lg-12 text-center">
                <div class="tahoma">Jakarta, {{ strftime("%d %B %Y", strtotime($dataCode->pelatihan[0]->tanggal_selesai->format('d F Y'))) }}</div>
                <div class="tahoma">{{ $dataCode->pelatihan[0]->user->ttd->unit }}</div>
                <img class="img-fluid" src="{{ asset('images/ttd/cap.png') }}" width="45%" style="position:absolute; right:190px; top:10px; z-index: 99;">
                {{-- <img class="img-fluid" src="{{ asset($data_code->ttd_path) }}" width="130" style="position:absolute; right:50px; top:0px;"> --}}
                @if($dataCode->pelatihan[0]->user->ttd->code_user == 'D' && \Carbon\Carbon::parse($dataCode->pelatihan[0]->created_at)->format('Y-m-d') < \Carbon\Carbon::parse('2022-10-05')->format('Y-m-d'))
                    <img class="img-fluid" src="{{ asset('images/ttd/eddy_satriya.png') }}" width="55%" style="position:absolute; right:70px; top: 0px;">
                    <br><br><br><br>
                    <div class="tahoma">Eddy Satriya</div>
                @else
                    <img class="img-fluid" src="{{ asset($dataCode->pelatihan[0]->user->ttd->ttd_path) }}" width="55%" style="position:absolute; right:70px; top: 0px;">
                    <br><br><br><br>
                    <div class="tahoma">{{ $dataCode->pelatihan[0]->user->ttd->name }}</div>
                @endif
            </div>
        </div>
    @endforeach

        <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
</html>