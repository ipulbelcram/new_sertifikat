<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table = 'pendidikan';

    protected $fillable = [
    	'pendidikan'
    ];

    public function data_code()
    {
    	return $this->hasMany('App\DataCode', 'pendidikan_terakhir_id');
    }
}
