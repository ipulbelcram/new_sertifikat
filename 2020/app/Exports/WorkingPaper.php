<?php

namespace App\Exports;

use App\DataCode;
use App\Pelatihan;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class WorkingPaper implements FromView, ShouldAutoSize, WithEvents
{
    public function __construct(string $pelatihan_id)
    {
        $this->pelatihan_id = $pelatihan_id;
    }
       
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:N1'; // All headers
                $cellRange2 = 'A2:N2'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
            },
        ];
    }

    public function view(): View
    {
        return view('excel.download_laporan', [
            // 'data' => DataCode::all()->where('pelatihan_id', $this->pelatihan_id),
            'data' => DB::table('vw_all_data_code')->where('pelatihan_id', $this->pelatihan_id)->get(),
            'data_pelatihan' => Pelatihan::all()->where('id', $this->pelatihan_id),
            'jumlah_data' => DataCode::all()->where('pelatihan_id', $this->pelatihan_id)->count()
        ]);
    }
}