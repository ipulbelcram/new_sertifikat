@extends('dashboard-template')
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-8">
            <h4 class="font-weight-bold">PENYEBARAN PESERTA PELATIHAN</h4>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <!-- PER DEPUTI -->
        <div class="col-lg-12 col-md-12">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">A. Per Deputi</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Deputi</th>
                            <th>Jumlah Pelatihan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_pelatihan_asdep = 0;
                                $total_peserta_asdep = 0;
                                $total_persentase_asdep = 0;
                            ?>
                            @foreach($per_asisten_deputi as $asisten_deputi)
                            <?php
                                $total_pelatihan_asdep += $asisten_deputi->jumlah_pelatihan;
                                $total_peserta_asdep += $asisten_deputi->jumlah_data_peruser;
                                $total_persentase_asdep += $asisten_deputi->persentase;
                            ?>
                            <tr>
                                <td><a href="{{ route('dashboard2.dataByUser', [$asisten_deputi->user_id]) }}">{{ $asisten_deputi->name }}</a></td>    
                                <td class="text-center">{{ number_format($asisten_deputi->jumlah_pelatihan,0,',','.') }}</td>
                                <td class="text-center">{{ number_format($asisten_deputi->jumlah_data_peruser,0,',','.') }}</td>
                                <td class="text-center">{{ $asisten_deputi->persentase }} %</td>
                            </tr>
                            @endforeach
                            <tr class="text-center font-weight-bold">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_pelatihan_asdep,0,',','.') }}</td>
                                <td>{{ number_format($total_peserta_asdep,0,',','.') }}</td>
                                <td>{{ round($total_persentase_asdep) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- PER PROVINSI -->
        <div class="col-lg-12 col-md-12 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0 font-weight-bold" style="color:#007bff">B. Per Provinsi/ Kab/ Kota</h6>
                </div>
                <div class="card-body p-0">
                    <table class="table table-bordered m-0">
                        <thead class="text-center">
                            <th>Provinsi</th>
                            <th>Jumlah Kab/ Kota</th>
                            <th>Jumlah Pelatihan</th>
                            <th>Jumlah Peserta</th>
                            <th>Persentase</th>
                        </thead>
                        <tbody>
                            <?php
                                $total_kab_kota_prov = 0;
                                $total_pelatihan_prov = 0;
                                $total_peserta_prov = 0;
                                $persentase_prov = 0;
                            ?>
                            @foreach($per_provinsi as $per_provinsi)
                            <?php
                                $total_kab_kota_prov += $per_provinsi->total_kab_kota;
                                $total_pelatihan_prov += $per_provinsi->jumlah_pelatihan;
                                $total_peserta_prov += $per_provinsi->jumlah_peserta;
                                $persentase_prov += $per_provinsi->persentase_jumlah_data;
                            ?>
                            <tr>
                                <td><a href="{{ route('dashboard2.pelatihanPerProvinsi', [$per_provinsi->provinsi_id]) }}">{{ $per_provinsi->provinsi }}</a></td>
                                <td class="text-center">{{ $per_provinsi->total_kab_kota }}</td>    
                                <td class="text-center">{{ number_format($per_provinsi->jumlah_pelatihan,0,',','.') }}</td>
                                <td class="text-center">{{ number_format($per_provinsi->jumlah_peserta,0,',','.') }}</td>
                                <td class="text-center">{{ $per_provinsi->persentase_jumlah_data }} %</td>
                            </tr>
                            @endforeach
                            <tr class="font-weight-bold text-center">
                                <td>TOTAL</td>
                                <td>{{ number_format($total_kab_kota_prov,0,',','.') }}</td>
                                <td>{{ number_format($total_pelatihan_prov,0,',','.') }}</td>
                                <td>{{ number_format($total_peserta_prov,0,',','.') }}</td>
                                <td>{{ round($persentase_prov) }} %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection