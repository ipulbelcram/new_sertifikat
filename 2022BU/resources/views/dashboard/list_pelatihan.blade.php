@extends('dashboard-template')
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-lg-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">List Pelatihan {{ !empty($provinsi_data) ? 'di Provinsi '.$provinsi_data[0]->provinsi : '' }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col">
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
            <h6 class="m-0">List Pelatihan</h6>
            </div>
            <div class="card-body p-0 pb-3">
            <table class="table mb-0">
                <thead class="bg-light">
                <tr>
                    <th scope="col" class="border-0" style="width:3%;">#</th>
                    <th scope="col" class="border-0" style="width:25%;">Nama Kegiatan</th>
                    <th scope="col" class="border-0" style="width:15%;">Kab/ Kota Kegiatan</th>
                    <th scope="col" class="border-0" style="width:23%;">Tanggal Pelaksanaan</th>
                    <th scope="col" class="border-0" style="width:12%;">Jumlah Peserta</th>
                    <th scope="col" class="border-0" style="width:12%;">User</th>
                </tr>
                </thead>
                <tbody>
                @php 
                    $no = ($pelatihan_data->perPage() * $pelatihan_data->currentPage()) - ($pelatihan_data->perPage() - 1);
                @endphp
                @foreach($pelatihan_data as $pelatihan)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td><a href="{{ url('dashboard2/'.$pelatihan->pelatihan_id.'/list_data_perpelatihan') }}">{{ $pelatihan->nama_pelatihan }}</a></td>
                    <td>{{ $pelatihan->kabupaten_kota }}</td>
                    <td>{{ strftime('%d %B %Y', strtotime($pelatihan->tanggal_mulai)) }} s.d {{ strftime('%d %B %Y', strtotime($pelatihan->tanggal_selesai)) }}</td>
                    <td class="text-center">{{ $pelatihan->jumlah_peserta }}</td>
                    <td>{{ $pelatihan->name }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <div class="card-footer border-top">
            <div class="row">
                <div class="col">
                {!! $pelatihan_data->links() !!}
                </div>
            </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@stop