<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\User;
use App\Pelatihan;
use App\DataCode;
use App\Provinsi;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminStatistik');
    }
    
    public function index() 
    {
        $jumlah_data_code = DataCode::all()->count();
        $total_pelatihan = Pelatihan::count();
        $jumlah_koperasi = DataCode::where('status_usaha_id', '25')->count();
        $jumlah_umkm = DataCode::where('status_usaha_id', '26')->count();
        $omset_usaha_koperasi = DataCode::where('status_usaha_id', '25')->select(DB::raw('SUM(omset_koperasi) as omset_koperasi'))->first();
        $omset_usaha_umkm = DataCode::where('status_usaha_id', '26')->select(DB::raw('SUM(omset_usaha_perbulan) as omset_usaha'))->first();
        $tenaga_kerja_koperasi = DataCode::where('status_usaha_id', '25')->select(DB::raw('SUM(tenaga_kerja_koperasi) as tenaga_kerja_koperasi'))->first();
        $tenaga_kerja_umkm = DataCode::where('status_usaha_id', '26')->select(DB::raw('SUM(tenaga_kerja_umkm) as tenaga_kerja_umkm'))->first();
        return view('dashboard/index', compact(
            'jumlah_data_code', 
            'total_pelatihan', 
            'jumlah_koperasi', 
            'jumlah_umkm', 
            'omset_usaha_koperasi', 
            'omset_usaha_umkm',
            'tenaga_kerja_koperasi',
            'tenaga_kerja_umkm'
        ));
    }

    public function profile_kegiatan_pelatihan()
    {
        $tujuan_pelaksanaan = DB::select("CALL TujuanPelaksanaan()");
        $dukungan_sektor = DB::select("CALL DukunganSektorPrioritas()");
        $metode_pelaksanaan = DB::select("CALL MetodePelaksanaanPelatihan");
        return view('dashboard.profile_kegiatan_pelatihan', compact(
            'tujuan_pelaksanaan',
            'dukungan_sektor',
            'metode_pelaksanaan'
        ));
    }

    public function profile_peserta_pelatihan()
    {
        $jenis_kelamin = DB::table('vw_jumlah_jenis_kelamin')->get();
        $status = DB::table('vw_jumlah_data_status')->get();
        $agama = DB::table('vw_jumlah_data_agama')->get();
        $pendidikan = DB::table('vw_jumlah_data_perpendidikan')->get();
        $pekerjaan_jabatan = DB::table('vw_jumlah_pekerjaan_jabatan')->get();
        $usia014 = DataCode::where('usia','<',14)->count();
        $usia1564 = DataCode::where([['usia','>',14], ['usia','<=',64]])->count();
        $usia65 = DataCode::where('usia','>',64)->count();
        $total_peserta =  DataCode::count();
        
        return view('dashboard.profile_peserta_pelatihan', compact(
            'jenis_kelamin',
            'status',
            'agama',
            'pendidikan',
            'pekerjaan_jabatan',
            'usia014',
            'usia1564',
            'usia65',
            'total_peserta'
        ));
    }

    public function profile_usaha_peserta()
    {   
        $status_usaha = DB::select("CALL StatusUsahaPeserta()");
        $sektor_usaha_peserta = DB::select("CALL SektorUsahaPeserta()");
        $jenis_usaha_koperasi = DB::select("CALL JenisUsahaKoperasiPeserta()");
        $jenis_usaha_umkm = DB::select("CALL JenisUsahaUmkmPeserta()");
        $lama_usaha = DB::select("CALL LamaUsahaUmkmPeserta()");
        return view('dashboard.profile_usaha_peserta', compact(
            'status_usaha',
            'sektor_usaha_peserta',
            'jenis_usaha_koperasi',
            'jenis_usaha_umkm',
            'lama_usaha'
        ));
    }

    public function penyebaran_peserta_pelatihan()
    {
        $per_asisten_deputi = DB::table('vw_jumlah_all_peruser')->where('user_level_id', 102)->get();
        $per_provinsi = DB::select("CALL JumlahAllPerprovinsi");
        return view('dashboard.penyebaran_peserta_pelatihan', compact('per_asisten_deputi', 'per_provinsi'));
    }

    public function dataByUser(request $request, $user_id) {
        $users_data = User::users()->get();
        //Result Pelatiha Per User
        $result_pelatihan = DB::table('vw_ket_pelatihan')->where('user_id', $user_id)->orderBy('tanggal_mulai', 'DESC')->paginate(8, ['*'], 'PelatihanPage');
        //Result user id
        $result_user = User::select('id', 'name')->where('id', $user_id)->get();

        return view('dashboard/dashboard_peruser', ['users_data' => $users_data, 'pelatihan_data' => $result_pelatihan, 'result_user' => $result_user ]);
    }

    public function list_data_perprovinsi($pelatihan_id) 
    {
        $users_data = User::users()->get();
        $data_pelatihan = Pelatihan::all()->where('id', $pelatihan_id);
        $data_code = DataCode::where('pelatihan_id', $pelatihan_id)->orderby('id', 'desc')->paginate(10);

        return view('dashboard/list_data_perpelatihan', compact('users_data','data_pelatihan', 'data_code'));
    }

    public function pelatihan_perprovinsi($provinsi_id) {
        $users_data = User::users()->get();
        $provinsi_data = Provinsi::where('id', $provinsi_id)->get();
        $pelatihan_data = DB::table('vw_ket_pelatihan')->where('provinsi_id', $provinsi_id)->orderby('tanggal_mulai', 'DESC')->paginate(8);

        return view('dashboard/list_pelatihan', compact('users_data','pelatihan_data', 'provinsi_data'));
    }

    public function pelatihan_provinsi_peruser ($user_id, $provinsi_id) 
    {   
        $users_data = User::users()->get();
        $provinsi_data = Provinsi::where('id', $provinsi_id)->get();
        $pelatihan_data = DB::table('vw_ket_pelatihan')->where(['user_id' => $user_id, 'provinsi_id' => $provinsi_id])->paginate(8);

        return view('dashboard/list_pelatihan', compact('users_data','pelatihan_data', 'provinsi_data'));
    }

    public function list_provinsi() 
    {
        $users_data = User::users()->get();
        $view_provinsi = DB::table('vw_jumlah_all_perprovinsi');
        $data_perprovinsi = $view_provinsi->get();
        $provinsi_data = $view_provinsi->pluck('jumlah_data_perprovinsi')->toJson();
        $provinsi_label = $view_provinsi->pluck('provinsi')->toJson();

        return view('dashboard/list_provinsi', compact('users_data', 'data_perprovinsi','provinsi_data', 'provinsi_label'));
    }

    public function detail_peserta($data_code_id) 
    {
        $users_data = User::users()->get();
        $data_peserta = DB::table('vw_all_data_code')->where('data_code_id', $data_code_id)->get();
        return view('dashboard.detail_peserta', compact('users_data','data_peserta'));
    }
}
